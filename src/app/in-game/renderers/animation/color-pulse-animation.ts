/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {ColourModes} from 'chromatism';
import {ColorHelper} from '../../../libraries/color-helper';
import {AnimationBase} from './animation-base';
import {IAnimationTarget} from './i-animation-target';
import RGB = ColourModes.RGB;

export class ColorPulseAnimation extends AnimationBase {
  public timePerFrame: number = 2;
  public numFrames: number = 16;

  private maxColor: RGB;
  private minColor: RGB;

  private animFrame: number = 0;
  private animFrameTimeLeft: number = this.timePerFrame;
  private animAscending: boolean = false;

  constructor(target: IAnimationTarget, maxColor: ColourModes.RGB, minColor: ColourModes.RGB) {
    super(target);

    this.maxColor = maxColor;
    this.minColor = minColor;
  }

  public update(delta: number): void {
    const range: RGB = {
      r: this.maxColor.r - this.minColor.r,
      g: this.maxColor.g - this.minColor.g,
      b: this.maxColor.b - this.minColor.b
    };

    let percentAnimated: number = this.animFrame / this.numFrames;
    if (!this.animAscending) {
      percentAnimated = 1 - this.animFrame / this.numFrames;
    }

    const actual: RGB = {
      r: Math.floor(this.minColor.r + range.r * percentAnimated),
      g: Math.floor(this.minColor.g + range.g * percentAnimated),
      b: Math.floor(this.minColor.b + range.b * percentAnimated)
    };

    if (this.animFrame === this.numFrames) {
      this.animAscending = !this.animAscending;
      this.animFrame = 0;
    } else {
      this.animFrameTimeLeft -= delta;
      if (this.animFrameTimeLeft <= 0) {
        this.animFrame++;
        this.animFrameTimeLeft = this.timePerFrame;
      }
    }

    if (this.target && this.target.tint) {
      this.target.tint = ColorHelper.getHexColorFromRGB(actual);
    }
  }
}
