/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {AnimationBase} from './animation-base';
import {IAnimationTarget} from './i-animation-target';

export class FadeInAnimation extends AnimationBase {
  private startOpacity: number;
  private endOpacity: number;
  private initialDelayRemaining: number;
  private initialDelay: number = 12;

  constructor(target: IAnimationTarget, startOpacity: number = 0, endOpacity: number = 1) {
    super(target);

    this.startOpacity = startOpacity;
    this.endOpacity = endOpacity;

    // target.alpha = startOpacity;
  }

  public update(delta: number): void {
    if (!this.target || delta <= 0 || this.target.alpha >= this.endOpacity) {
      return;
    }

    if (this.initialDelayRemaining > delta) {
      this.initialDelayRemaining -= delta;

      return;
    }
    this.initialDelayRemaining = 0;

    // const decay: number = 0.01 * delta;
    const alpha: number = delta * 0.025;
    this.target.alpha = Math.max(Math.min(this.target.alpha + alpha, this.endOpacity), 0);
  }

  public reset(): void {
    this.target.alpha = this.startOpacity;
    this.initialDelayRemaining = this.initialDelay;
  }
}
