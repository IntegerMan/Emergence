/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {IAnimationTarget} from './i-animation-target';

export abstract class AnimationBase {

  public target: IAnimationTarget;

  constructor(target: IAnimationTarget) {
    this.target = target;
  }

  public abstract update(delta: number): void;

}
