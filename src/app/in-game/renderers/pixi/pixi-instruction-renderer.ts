/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import {RenderInstruction} from '../../../libraries/rendering/render-instruction';
import {PixiRenderingDirector} from './pixi-rendering-director';

export abstract class PixiInstructionRenderer {
  public abstract getContainer(director: PixiRenderingDirector, instruction: RenderInstruction): PIXI.Container;

  public abstract render(director: PixiRenderingDirector, instruction: RenderInstruction, container: PIXI.Container): void;
}
