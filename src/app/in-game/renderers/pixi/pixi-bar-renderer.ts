/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import {ColorHelper} from '../../../libraries/color-helper';
import {ProgressBarRenderInstruction} from '../../../libraries/rendering/progress-bar-render-instruction';
import {RenderInstruction} from '../../../libraries/rendering/render-instruction';
import {PixiHelpers} from './pixi-helpers';
import {PixiInstructionRenderer} from './pixi-instruction-renderer';
import {PixiRenderingDirector} from './pixi-rendering-director';

export class PixiBarRenderer extends PixiInstructionRenderer {
  public getContainer(director: PixiRenderingDirector, instr: RenderInstruction): PIXI.Container {
    const container: PIXI.Container = new PIXI.Container();

    director.addContainer(container, instr.renderingLayer);

    return container;
  }

  public render(director: PixiRenderingDirector, instr: ProgressBarRenderInstruction, container: PIXI.Container): void {
    if (instr.tooltip) {
      PixiHelpers.addTooltipHandlers(container, instr, director.tooltipProvider);
    }

    if (instr.label) {
      const fontSize: number = 16;
      const text: PIXI.Text = new PIXI.Text(instr.label, PixiHelpers.getTextStyle(instr.foreColor, fontSize));

      if (instr.alignRight) {
        PixiHelpers.rightAlignText(text, instr.startPixel, instr.size);
      } else {
        text.position.set(instr.startPixel.x, instr.startPixel.y);
      }
      container.addChild(text);
    }

    const rect: PIXI.Graphics = new PIXI.Graphics();
    container.addChild(rect);

    // Draw the background color
    const backColor: number =
      instr.value >= instr.maxValue
        ? ColorHelper.getHexColorFromString(instr.bgColor)
        : ColorHelper.getHexColorFromRGB(ColorHelper.darkenRGB(instr.bgColor, 0.5));

    rect.beginFill(backColor);
    rect.drawRect(instr.startPixel.x, instr.startPixel.y + 25, instr.size.x, instr.size.y - 25);
    rect.endFill();

    // Draw the foreground based on the percent of the max value
    if (instr.value < instr.maxValue) {
      rect.beginFill(ColorHelper.getHexColorFromString(instr.bgColor));
      const percent: number = (instr.value - instr.minValue) / instr.maxValue;
      const endX: number = Math.round(instr.size.x * Math.min(1, Math.max(0, percent)));
      rect.drawRect(instr.startPixel.x, instr.startPixel.y + 25, endX, instr.size.y - 25);
      rect.endFill();
    }

    director.addAnimations(instr, rect, instr.bgColor);
  }
}
