/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import { ColorHelper } from '../../../libraries/color-helper';
import { MaterialColor } from '../../../libraries/material-color.enum';
import { CellRenderInstruction } from '../../../libraries/rendering/cell-render-instruction';
import { RenderInstruction } from '../../../libraries/rendering/render-instruction';
import { SpriteReference } from '../../../libraries/rendering/sprite-reference';
import { SpriteSheetReference } from '../../../libraries/rendering/sprite-sheet-reference.enum';
import { GameObject } from '../../../libraries/world/objects/game-object';
import { Point } from '../../../libraries/world/point';
import { FloorType } from '../../../services/generation-service/floor-type.enum';
import { Logger } from '../../../shared/utility/logger';
import { ColorPulseAnimation } from '../animation/color-pulse-animation';
import { FadeOutAnimation } from '../animation/fade-out-animation';
import { PixiHelpers } from './pixi-helpers';
import { PixiInstructionRenderer } from './pixi-instruction-renderer';
import { PixiRenderingDirector } from './pixi-rendering-director';
import InteractionEvent = PIXI.interaction.InteractionEvent;

export class PixiCellRenderer extends PixiInstructionRenderer {
  private static lastCursor: Point = new Point(0, 0);

  private isPressed: boolean = false;
  private isMouseOver: boolean = false;

  private cursor: PIXI.Sprite;

  public getContainer(director: PixiRenderingDirector, instruction: CellRenderInstruction): PIXI.Container {
    let container: PIXI.Container = null;

    if (!container) {
      container = this.createContainer(instruction, director);

      container.interactive = true;
      director.addContainer(container, instruction.renderingLayer);
    } else {
      container.removeChildren();
    }

    return container;
  }

  public render(director: PixiRenderingDirector, instr: CellRenderInstruction, container: PIXI.Container): void {
    // Set up the tooltip now
    if (instr.tooltip) {
      PixiHelpers.addTooltipHandlers(container, instr, director.tooltipProvider);
    }

    // Figure out what was at the location we're rendering. If it's nothing, then buzz off
    const floorType: FloorType = instr.cell.floorType;
    if (floorType === FloorType.Void) {
      return;
    }

    // Figure out our tint
    const shouldShade: boolean = !instr.context.isVisible && instr.context.isKnown;

    // Render floor
    if (instr.cell.spriteReference) {
      const sprite: PIXI.Sprite = PixiHelpers.buildSprite(
        director,
        instr.cell.spriteReference,
        instr.size,
        this.getEffectiveTint(instr.cell.spriteReference.tint, shouldShade)
      );
      container.addChild(sprite);
    }

    // Render objects
    for (const obj of instr.cell.objects.slice(0).sort((a: GameObject, b: GameObject): number => a.zIndex - b.zIndex)) {
      const sprite: PIXI.Sprite = PixiHelpers.buildSprite(
        director,
        obj.spriteReference,
        instr.size,
        this.getEffectiveTint(obj.spriteReference.tint, shouldShade)
      );

      if (obj.pulseDuration > 0) {
        const anim: ColorPulseAnimation = new ColorPulseAnimation(
          sprite,
          ColorHelper.getRGBFromWeb(obj.foregroundColor),
          ColorHelper.darkenRGB(obj.foregroundColor, 0.1)
        );
        anim.timePerFrame = obj.pulseDuration;
        director.animations.push(anim);
      }

      container.addChild(sprite);
    }

    // Add a red flash to indicate something got hurt last turn if applicable
    if (
      instr.context.isVisible &&
      instr.cell.actor &&
      instr.cell.actor.wasHurtLastTurn &&
      instr.context.isInitialRender &&
      !instr.isInToolTip
    ) {
      const damageRect: PIXI.Graphics = PixiHelpers.buildPixiRectangle(new Point(0, 0), instr.size, true, MaterialColor.redDark4);
      damageRect.name = `${instr.id}_damage_marker`;
      container.addChild(damageRect);

      director.animations.push(new FadeOutAnimation(damageRect, 5));
    }

    // If this cell is the cell the POV actor has marked, let's give it an outline
    if (instr.cell.pos.equals(instr.context.actor.markedPos) && !instr.isInToolTip) {
      const markerRect: PIXI.Graphics = PixiHelpers.buildPixiRectangle(new Point(0, 0), instr.size, false, MaterialColor.yellow);
      markerRect.name = `${instr.id}_marker_rect`;
      container.addChild(markerRect);
    }

    if (instr.cell.actor && instr.context.isVisible) {
      const spriteRef: SpriteReference = instr.cell.actor.spriteReference;

      const sprite: PIXI.Sprite = PixiHelpers.buildSprite(director, spriteRef, instr.size, instr.cell.actor.spriteReference.tint);
      container.addChild(sprite);

      if (spriteRef.tint) {
        // Try out an effect where the actor will pulse more frequently the more damaged they are
        // const pulseDuration = Math.max(0.01, instr.cell.actor.currentHp / instr.cell.actor.maxHp * 3);
        const anim: ColorPulseAnimation = new ColorPulseAnimation(
          sprite,
          ColorHelper.getRGBFromWeb(spriteRef.tint),
          ColorHelper.darkenRGB(spriteRef.tint, 0.5)
        );
        anim.timePerFrame = anim.timePerFrame * (instr.cell.actor.currentHp / instr.cell.actor.maxHp);

        director.animations.push(anim);
      }
    }

    if (!instr.isInToolTip) {
      this.drawCursor(director, instr, container);
    }
  }

  private drawCursor(director: PixiRenderingDirector, instr: CellRenderInstruction, container: PIXI.Container): void {
    // Draw a cursor border if the mouse is over it
    this.cursor = PixiHelpers.buildSprite(
      director,
      new SpriteReference(SpriteSheetReference.small, 3, 2, instr.cursorColor),
      instr.size,
      instr.cursorColor
    );
    this.cursor.name = `${instr.id}_cursor`;
    this.cursor.alpha = 0.85;
    this.cursor.visible = this.isMouseOver;
    container.addChild(this.cursor);

    const cursorAnim: ColorPulseAnimation = new ColorPulseAnimation(
      this.cursor,
      ColorHelper.getRGBFromWeb(instr.cursorColor),
      ColorHelper.darkenRGB(instr.cursorColor, 0.15)
    );
    cursorAnim.timePerFrame = 1;
    director.animations.push(cursorAnim);
  }

  private getEffectiveTint(tint: string, shouldShade: boolean): string {
    let output: string = tint ? tint : MaterialColor.white;
    if (shouldShade) {
      output = ColorHelper.getFadedColor(output);
    }

    return output;
  }

  private createContainer(instr: CellRenderInstruction, director: PixiRenderingDirector): PIXI.Container {
    const container: PIXI.Container = new PIXI.Container();
    container.name = instr.id;
    if (!instr.isInToolTip) {
      container.position.set(instr.context.screenPos.x, instr.context.screenPos.y);
    } else {
      container.position.set(instr.startPixel.x, instr.startPixel.y);
    }

    // When click function is defined, we need to care about mouse click events
    if (instr.clickFunction) {
      container.buttonMode = true;
      container.on('pointerdown', () => this.onMouseButtonDown(instr, director));
      container.on('pointerup', () => this.onMouseButtonUp(instr, director));
      container.on('pointerupoutside', () => this.onMouseButtonUpOutside(instr, director));
    }

    // Everything should care about enter / exit events for cursor tracking
    container.on('pointerover', (e: any) => this.onMouseEnter(instr, director, e));
    container.on('pointerout', () => this.onMouseLeave(instr, director));

    return container;
  }

  private onMouseEnter(instr: RenderInstruction, director: PixiRenderingDirector, e: InteractionEvent): void {
    this.isMouseOver = true;
    this.isPressed = false;

    Logger.debug(e.data.global, PixiCellRenderer.lastCursor);

    if (instr.tooltip && (PixiCellRenderer.lastCursor.x !== e.data.global.x || PixiCellRenderer.lastCursor.y !== e.data.global.y)) {
      // Don't just copy over the cursor; we'll want to be sure we use a different reference
      PixiCellRenderer.lastCursor.x = e.data.global.x;
      PixiCellRenderer.lastCursor.y = e.data.global.y;

      director.tooltipProvider.requestTooltip(instr);
    }

    this.cursor.visible = true;
  }

  private onMouseLeave(instr: RenderInstruction, director: PixiRenderingDirector): void {
    this.isMouseOver = false;
    this.isPressed = false;
    if (instr.tooltip) {
      director.tooltipProvider.cancelTooltip(instr);
    }
    this.cursor.visible = false;
  }

  private onMouseButtonUpOutside(instr: RenderInstruction, director: PixiRenderingDirector): void {
    this.isPressed = false;
  }

  private onMouseButtonUp(instr: RenderInstruction, director: PixiRenderingDirector): void {
    if (this.isPressed) {
      PixiHelpers.handleClickEvent(instr);
    }
    this.isPressed = false;
  }

  private onMouseButtonDown(instr: RenderInstruction, director: PixiRenderingDirector): void {
    this.isPressed = true;
  }
}
