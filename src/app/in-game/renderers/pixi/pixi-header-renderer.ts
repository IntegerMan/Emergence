/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import { MaterialColor } from '../../../libraries/material-color.enum';
import { HeaderInstruction } from '../../../libraries/rendering/header-instruction';
import { RenderInstruction } from '../../../libraries/rendering/render-instruction';
import { Point } from '../../../libraries/world/point';
import { PixiHelpers } from './pixi-helpers';
import { PixiInstructionRenderer } from './pixi-instruction-renderer';
import { PixiRenderingDirector } from './pixi-rendering-director';

export class PixiHeaderRenderer extends PixiInstructionRenderer {
  private container: PIXI.Container;

  public getContainer(director: PixiRenderingDirector, instruction: RenderInstruction): PIXI.Container {
    if (!this.container) {
      this.container = new PIXI.Container();
      this.container.name = 'HeaderContainer';
      director.addContainer(this.container, instruction.renderingLayer);
    } else {
      this.container.removeChildren();
    }

    return this.container;
  }

  public render(director: PixiRenderingDirector, instr: HeaderInstruction, container: PIXI.Container): void {
    const background: PIXI.Graphics = PixiHelpers.buildPixiRectangle(instr.startPixel, instr.size, true, instr.bgColor);
    background.name = 'HeaderBG';
    container.addChild(background);

    const shadow: PIXI.Graphics = PixiHelpers.buildPixiRectangle(
      instr.startPixel.offset(0, background.height),
      new Point(instr.size.x, 3),
      true,
      MaterialColor.greyDark3
    );
    shadow.name = 'HeaderShadow';
    shadow.alpha = 0.6;
    container.addChild(shadow);

    const title: PIXI.Text = new PIXI.Text(instr.name, PixiHelpers.getTextOptions(MaterialColor.white, 24));
    title.position.set(10, 10);
    container.addChild(title);

    const version: PIXI.Text = new PIXI.Text(instr.version, PixiHelpers.getTextOptions(MaterialColor.white, 12));
    version.position.set(title.width + 18, title.height - version.height + 8);
    container.addChild(version);
  }
}
