/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as PIXI from 'pixi.js';
import { TextAlignment } from '../../../libraries/rendering/text-alignment';
import { TextRenderInstruction } from '../../../libraries/rendering/text-render-instruction';
import { PixiHelpers } from './pixi-helpers';
import { PixiInstructionRenderer } from './pixi-instruction-renderer';
import { PixiRenderingDirector } from './pixi-rendering-director';

export class PixiTextRenderer extends PixiInstructionRenderer {
  private container: PIXI.Container;

  public getContainer(director: PixiRenderingDirector, instruction: TextRenderInstruction): PIXI.Container {

    if (!this.container) {
      this.container = director.textRenderingContainer;
    }

    if (!this.container) {
      this.container = new PIXI.Container();
      this.container.name = 'TextContainer';
      director.addContainer(this.container, instruction.renderingLayer);
      director.textRenderingContainer = this.container;
    } else {
      this.container.removeChildren();
    }

    return this.container;
  }

  public render(director: PixiRenderingDirector, instr: TextRenderInstruction, container: PIXI.Container): void {
    const text: PIXI.Text = new PIXI.Text(instr.text, PixiHelpers.getTextOptions(instr.bgColor, instr.fontSize));
    container.addChild(text);

    switch (instr.alignment) {
      case TextAlignment.center:
        PixiHelpers.centerText(text, instr.startPixel, instr.size);
        break;

      case TextAlignment.right:
        PixiHelpers.rightAlignText(text, instr.startPixel, instr.size);
        break;

      default:
        text.position.set(instr.startPixel.x, instr.startPixel.y);
    }

    if (instr.tooltip) {
      PixiHelpers.addTooltipHandlers(text, instr, director.tooltipProvider);
    }

    director.addAnimations(instr, text, instr.bgColor);
  }
}
