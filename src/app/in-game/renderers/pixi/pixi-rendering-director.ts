/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ColourModes } from 'chromatism';
import * as PIXI from 'pixi.js';
import { Subscription } from 'rxjs/Subscription';
import { ColorHelper } from '../../../libraries/color-helper';
import { IRenderingDirector } from '../../../libraries/rendering/i-rendering-director';
import { ITooltipProvider } from '../../../libraries/rendering/i-tooltip-provider';
import { RenderInstruction } from '../../../libraries/rendering/render-instruction';
import { RenderingLayer } from '../../../libraries/rendering/rendering-layer.enum';
import { SpriteReference } from '../../../libraries/rendering/sprite-reference';
import { Point } from '../../../libraries/world/point';
import { IInitializable } from '../../../services/i-initializable';
import { PixiTextureService } from '../../../services/rendering-service/pixi-texture.service';
import { GoogleAnalyticsHelper } from '../../../shared/utility/google-analytics-helper';
import { AnimationBase } from '../animation/animation-base';
import { ColorPulseAnimation } from '../animation/color-pulse-animation';
import { FadeOutAnimation } from '../animation/fade-out-animation';
import { IRendererProvider } from './i-renderer-provider';
import { PixiInstructionRenderer } from './pixi-instruction-renderer';
import IApplicationOptions = PIXI.IApplicationOptions;

export class PixiRenderingDirector implements IRenderingDirector, IInitializable {
  public static tileSize: number = 32;

  public tooltipProvider: ITooltipProvider;
  public clientSize: Point;
  public animations: AnimationBase[];
  public app: PIXI.Application;
  public rendererProvider: IRendererProvider;

  public tooltipContainer: PIXI.Container;
  public logContainer: PIXI.Container;
  public textRenderingContainer: PIXI.Container;

  private view: HTMLCanvasElement;

  private zoomLevel: number = 1;

  private instructionsOnLoad: RenderInstruction[];
  private readonly containers: PIXI.Container[];
  private readonly textureLoadSub: Subscription;

  constructor(
    private canvas: HTMLCanvasElement,
    private textureService: PixiTextureService,
    tooltipProvider: ITooltipProvider,
    rendererProvider: IRendererProvider,
    private forceCanvas: boolean = false
  ) {
    this.containers = [];
    this.rendererProvider = rendererProvider;
    this.tooltipProvider = tooltipProvider;

    this.initialize();

    // Queue up the texture load process
    if (this.textureService.isLoaded) {
      this.onLoaded(true);
    } else {
      this.textureLoadSub = this.textureService.loaded.subscribe((isLoaded: boolean): void => this.onLoaded(isLoaded));
      this.textureService.load();
    }
  }

  public render(instructions: RenderInstruction[]): void {
    if (!this.textureService.isLoaded) {
      this.instructionsOnLoad = instructions;

      return;
    }

    // Ensure we clear before render
    this.clear();

    for (const instr of instructions) {
      const renderer: PixiInstructionRenderer = this.rendererProvider.getRendererForInstruction(instr);
      if (renderer) {
        const container: PIXI.Container = renderer.getContainer(this, instr);
        renderer.render(this, instr, container);
      }
    }
  }

  public getSprite(spriteReference: SpriteReference): PIXI.Sprite {
    return this.textureService.getSprite(spriteReference);
  }

  public handleWidthInvalidated(width: number, height: number, zoomLevel: number): void {
    // Copy over the width / height that we're using

    this.clientSize = new Point(width, height);

    this.view.style.width = `${width}px`;
    this.view.style.height = `${height}px`;

    this.app.renderer.resize(width, height);

    this.zoomLevel = zoomLevel;
  }

  public clear(): void {
    // Animations will be regenerated
    this.animations.length = 0;

    // Clear the HUD
    this.containers[RenderingLayer.commandBar].removeChildren();
    this.containers[RenderingLayer.systemButtons].removeChildren();
    this.containers[RenderingLayer.level].removeChildren();
  }

  public addContainer(container: PIXI.Container, layer: RenderingLayer): void {
    if (!this.containers[layer]) {
      throw new Error(`Attempted to add a container to layer ${layer} but it hadn't been instantiated yet.`);
    }

    this.containers[layer].addChild(container);
  }

  public addAnimations(instr: RenderInstruction, effect: PIXI.Graphics | PIXI.Sprite, pulseColor: string): void {
    if (instr.fadeRate > 0) {
      this.animations.push(new FadeOutAnimation(effect, instr.fadeRate));
    }

    if (instr.pulseDuration > 0) {
      const baseColor: ColourModes.RGB = ColorHelper.getRGBFromWeb(pulseColor);
      const pulseColorRGB: ColourModes.RGB = ColorHelper.darkenRGB(pulseColor, 0.5);
      const anim: ColorPulseAnimation = new ColorPulseAnimation(effect, baseColor, pulseColorRGB);
      anim.timePerFrame = instr.pulseDuration;
      this.animations.push(anim);
    }
  }

  private onTick(delta: number): void {
    for (const animation of this.animations) {
      animation.update(delta);
    }
  }

  private addRootContainer(id: RenderingLayer): void {
    const container: PIXI.Container = new PIXI.Container();
    container.name = `Container_${id}`;

    this.containers[id] = container;
    this.app.stage.addChild(container);
  }

  private onLoaded(isLoaded: boolean): void {
    if (isLoaded && this.instructionsOnLoad && this.instructionsOnLoad.length > 0) {
      this.render(this.instructionsOnLoad);
    }

    if (this.textureLoadSub) {
      this.textureLoadSub.unsubscribe();
    }
  }

  public initialize(): void {
    const options: IApplicationOptions = {
      backgroundColor: 0,
      clearBeforeRender: true,
      view: this.canvas,
      transparent: false,
      resolution: 1,
      autoResize: false,
      antialias: false // This makes a HUGE difference in text rendering performance
    };

    // Without this, corners just don't look right
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

    this.app = new PIXI.Application(800, 600, options, this.forceCanvas);
    this.app.ticker.add((delta: number): void => this.onTick(delta));

    if (this.app.renderer instanceof PIXI.CanvasRenderer) {
      GoogleAnalyticsHelper.emitEvent('ClientInfo', 'Renderer', 'PixiCanvas');
    } else {
      GoogleAnalyticsHelper.emitEvent('ClientInfo', 'Renderer', 'PixiWebGL');
    }

    this.addRootContainer(RenderingLayer.level);
    this.addRootContainer(RenderingLayer.commandBar);
    this.addRootContainer(RenderingLayer.header);
    this.addRootContainer(RenderingLayer.systemButtons);
    this.addRootContainer(RenderingLayer.log);
    this.addRootContainer(RenderingLayer.prompt);
    this.addRootContainer(RenderingLayer.tooltip);

    this.animations = [];

    this.view = this.app.view;
    this.view.style.width = '100%';
    this.view.style.height = '100%';
  }
}
