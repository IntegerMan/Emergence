/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ButtonInstruction } from '../../libraries/rendering/button-instruction';
import { TooltipService } from '../../services/ui-service/tooltip.service';
import { Logger } from '../utility/logger';

@Component({
  selector: 'em-inline-button',
  templateUrl: './inline-button.component.html',
  styleUrls: ['./inline-button.component.scss']
})
export class InlineButtonComponent implements OnInit {
  @Input() public iconName: string;
  @Input() public instruction: ButtonInstruction;
  @Input() public isAccent: boolean = false;
  @Input() public title: string = '';
  @Input() public link: string[] = null;

  constructor(private tooltipService: TooltipService, private router: Router) {}

  public ngOnInit(): void {}

  public onClick(): void {
    Logger.debug(`Inline Button Clicked`, this);

    if (this.instruction && this.instruction.isEnabled && this.instruction.clickFunction) {
      this.instruction.clickFunction();
    } else if (this.link) {
      this.router.navigate(this.link);
    }
  }

  public onMouseEnter(): void {
    if (this.instruction && this.instruction.isEnabled) {
      this.tooltipService.requestTooltip(this.instruction);
    }
  }

  public onMouseLeave(): void {
    if (this.instruction && this.instruction.isEnabled) {
      this.tooltipService.cancelTooltip(this.instruction);
    }
  }
}
