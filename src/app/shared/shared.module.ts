/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HelpLinkDirective } from 'app/help/help-link.directive';
import { AppRoutingModule } from '../app-routing.module';
import { HelpParagraphComponent } from '../help/help-paragraph/help-paragraph.component';
import { RenderingServiceModule } from '../services/rendering-service/rendering-service.module';
import { UiServiceModule } from '../services/ui-service/ui-service.module';
import { ActorDefinitionStatsHelpComponent } from './actor-definition-stats-help/actor-definition-stats-help.component';
import { CommandSlotComponent } from './command-slot/command-slot.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { InlineButtonComponent } from './inline-button/inline-button.component';
import { MaterialModule } from './material/material.module';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
    // Base Angular Imports
    BrowserModule,
    BrowserAnimationsModule,
    // Routing
    RouterModule,
    AppRoutingModule,
    // Engine Bits
    RenderingServiceModule,
    UiServiceModule
  ],
  entryComponents: [ConfirmDialogComponent],
  declarations: [
    CommandSlotComponent,
    NotFoundComponent,
    InlineButtonComponent,
    ConfirmDialogComponent,
    HelpParagraphComponent,
    HelpLinkDirective,
    ActorDefinitionStatsHelpComponent
  ],
  exports: [
    CommandSlotComponent,
    NotFoundComponent,
    InlineButtonComponent,
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
    ConfirmDialogComponent,
    HelpParagraphComponent,
    ActorDefinitionStatsHelpComponent
  ]
})
export class SharedModule {}
