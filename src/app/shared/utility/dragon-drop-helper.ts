/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {ActiveCommand} from '../../libraries/world/actors/commands/active-command';
import {ActorCommand} from '../../libraries/world/actors/commands/actor-command';
import {CommandSlot} from '../../libraries/world/actors/commands/command-slot';

/**
 * Utility class containing helpful methods for working with Drag and Drop operations within the context of this project's domain model.
 */
export class DragonDropHelper {
  /**
   * Responds to a command drag and drop event by swapping commands as needed
   * @param {CommandSlot} target the command slot that was dropped onto
   * @param {CommandSlot} source the command slot that was dragged
   */
  public static handleCommandDragDrop(target: CommandSlot, source: CommandSlot): void {
    // Swap the commands
    const sourceCommand: ActorCommand = source.command;
    source.command = target.command;
    target.command = sourceCommand;

    // Ensure active commands are deactivated during the move
    DragonDropHelper.deactivateCommandAsNeeded(source);
    DragonDropHelper.deactivateCommandAsNeeded(target);
  }

  /**
   * Deactivates the command if it is an active command that is currently active
   * @param {CommandSlot} slot the command slot to evaluate.
   */
  private static deactivateCommandAsNeeded(slot: CommandSlot): void {

    // Don't deactivate if we're in the command bar
    if (slot.isInCommandBar) {
      return;
    }

    const command: ActorCommand = slot.command;

    if (command && (<ActiveCommand>command).isActive) {
      (<ActiveCommand>command).isActive = false;
    }
  }
}
