/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { environment } from '../../../environments/environment';

export class Logger {
  public static errorLogged: EventEmitter<string> = new EventEmitter<string>();

  private static performInvoke(message: any[], methodName: string): void {
    if (!isNullOrUndefined(console) && !isNullOrUndefined(console[methodName])) {
      console[methodName](...message);
    }
  }

  public static error(...message: any[]): void {
    if (message && message[0]) {
      Logger.errorLogged.emit(message[0]);
    }
    this.performInvoke(message, 'error');
  }

  public static debug(...message: any[]): void {
    if (!environment.production) {
      this.performInvoke(message, 'debug');
    }
  }

  public static warn(...message: any[]): void {
    this.performInvoke(message, 'warn');
  }

  public static info(...message: any[]): void {
    this.performInvoke(message, 'info');
  }

  public static log(...message: any[]): void {
    this.performInvoke(message, 'log');
  }
}
