/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {DefaultUrlSerializer, UrlTree} from '@angular/router';
import {GoogleAnalyticsHelper} from './google-analytics-helper';

export class LowerCaseUrlSerializer extends DefaultUrlSerializer {
  public parse(url: string): UrlTree {
    const sanitized: string = url.toLowerCase();

    GoogleAnalyticsHelper.emitScreenView(sanitized);

    return super.parse(sanitized);
  }
}
