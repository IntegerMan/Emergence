/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CharacterClass } from '../libraries/setup/character-class';
import { GameSetupService } from '../services/core-engine/game-setup.service';
import { Logger } from './utility/logger';

@Injectable()
export class CharacterClassResolver implements Resolve<CharacterClass> {
  constructor(private setupService: GameSetupService, private router: Router) {}

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<CharacterClass> | Promise<CharacterClass> | CharacterClass {
    const classId: string = route.paramMap.get('classId');
    Logger.debug(`Character class resolver looking for character class named ${classId}`);

    const character: CharacterClass = this.setupService.getClass(classId);

    if (!character) {
      this.router.navigate(['/not-found']);
    }

    return character;
  }
}
