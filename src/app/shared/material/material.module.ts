/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatStepperModule,
  MatTabsModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    // Angular Material
    MatExpansionModule,
    MatTabsModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    MatStepperModule
  ],
  declarations: [],
  exports: [MatExpansionModule, MatTabsModule, MatIconModule, MatInputModule, MatFormFieldModule, MatGridListModule, MatStepperModule]
})
export class MaterialModule {}
