/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, Input, OnInit } from '@angular/core';
import { IActorDefinition } from '../../libraries/world/actors/i-actor-definition';

@Component({
  selector: 'em-actor-definition-stats-help',
  templateUrl: './actor-definition-stats-help.component.html',
  styleUrls: ['./actor-definition-stats-help.component.scss']
})
export class ActorDefinitionStatsHelpComponent implements OnInit {
  @Input() public actorDefinition: IActorDefinition;

  constructor() {}

  public ngOnInit(): void {}

  public getDamageRangeText(): string {
    if (this.actorDefinition.minAttack === this.actorDefinition.maxAttack) {
      return `${this.actorDefinition.minAttack}`;
    }

    return `${this.actorDefinition.minAttack} - ${this.actorDefinition.maxAttack}`;
  }
}
