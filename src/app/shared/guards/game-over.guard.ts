/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {GameOverInfo} from '../../libraries/game-over-info';
import {GameStatus} from '../../libraries/game-status';
import {Randomizer} from '../../libraries/randomizer';
import {GameLoopService} from '../../services/core-engine/game-loop.service';
import {GameSetupService} from '../../services/core-engine/game-setup.service';
import {Logger} from '../utility/logger';

@Injectable()
export class GameOverGuard implements CanActivate {
  constructor(
    private gameLoopService: GameLoopService,
    private setupService: GameSetupService,
    private router: Router
  ) {}

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // If the game is not over, jump to the in-game page
    const gameState: GameStatus = this.gameLoopService.gameState;
    if (gameState !== GameStatus.gameOver) {
      if (environment.allowDebugMode) {
        // Build a mostly-randomized entity
        const info: GameOverInfo = new GameOverInfo();
        info.isVictory = Randomizer.getRandomBoolean();
        info.damageDealt = Randomizer.getRandomNumber(0, 1000000);
        info.damageReceived = Randomizer.getRandomNumber(0, 1000000);
        info.numKills = Randomizer.getRandomNumber(0, 1000000);
        info.score = Randomizer.getRandomNumber(0, 100000000);
        info.turnsTaken = Randomizer.getRandomNumber(0, 1000000);

        Logger.warn(`Navigated to game over while a game is not currently over. Setting a random game over info object.`);

        this.gameLoopService.endGame(info);
      } else {
        Logger.warn(`Attempted to navigate, but GameOverGuard rejected it. Current game state ${gameState}`);

        this.router.navigate(['/play']);

        return false;
      }
    }

    return true;
  }
}
