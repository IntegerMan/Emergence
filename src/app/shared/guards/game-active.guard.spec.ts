/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */
import {inject, TestBed} from '@angular/core/testing';

import {GameActiveGuard} from './game-active.guard';

describe('GameActiveGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameActiveGuard]
    });
  });

  it(
    'should ...',
    inject([GameActiveGuard], (guard: GameActiveGuard) => {
      expect(guard).toBeTruthy();
    })
  );
});
