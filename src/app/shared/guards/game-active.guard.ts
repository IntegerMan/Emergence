/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Randomizer } from '../../libraries/randomizer';
import { CharacterClass } from '../../libraries/setup/character-class';
import { GameLoopService } from '../../services/core-engine/game-loop.service';
import { GameSetupService } from '../../services/core-engine/game-setup.service';
import { Logger } from '../utility/logger';

@Injectable()
export class GameActiveGuard implements CanActivate {
  constructor(private gameLoopService: GameLoopService, private setupService: GameSetupService, private router: Router) {}

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // If the game is not active, jump to the setup page
    if (!this.gameLoopService.isInGame) {
      if (environment.allowDebugMode) {
        const characterClass: CharacterClass = Randomizer.getRandomEntry(this.setupService.availableClasses);

        Logger.warn(`Navigated to game without a game state. Starting a new game as ${characterClass.name}`);

        this.gameLoopService.start(characterClass);
      } else {
        Logger.warn(`Attempted to navigate, but GameActiveGuard rejected it. Current game state ${this.gameLoopService.gameState}`);

        this.router.navigate(['/setup']);

        return false;
      }
    }

    return true;
  }
}
