/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {GameOverGuard} from './game-over.guard';

describe('GameOverGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameOverGuard]
    });
  });

  it('should ...', inject([GameOverGuard], (guard: GameOverGuard) => {
    expect(guard).toBeTruthy();
  }));
});
