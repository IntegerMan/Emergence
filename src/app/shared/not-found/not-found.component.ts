/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleAnalyticsHelper } from '../utility/google-analytics-helper';

@Component({
  selector: 'em-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {
  constructor(private route: Router) {}
  public resource: string = 'The requested resource';

  public ngOnInit(): void {
    this.resource = this.route.url;
    GoogleAnalyticsHelper.emitEvent('Navigation', 'NotFound', this.resource);
  }
}
