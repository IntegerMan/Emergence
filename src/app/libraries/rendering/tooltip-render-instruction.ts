/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Point} from '../world/point';
import {RenderInstruction} from './render-instruction';
import {RenderingLayer} from './rendering-layer.enum';
import {TooltipInfo} from './tooltip-info';

export class TooltipRenderInstruction extends RenderInstruction {
  public foreColor: string;

  public parent: RenderInstruction;
  public renderParent: boolean = false;

  constructor(parent: RenderInstruction, bgColor: string, foreColor: string) {
    super(parent ? parent.startPixel : new Point(0, 0), parent ? parent.size : new Point(0, 0), bgColor);

    this.foreColor = foreColor;
    this.parent = parent;
    this.renderingLayer = RenderingLayer.tooltip;
  }

  public get info(): TooltipInfo {
    return this.parent ? this.parent.tooltip : null;
  }
}
