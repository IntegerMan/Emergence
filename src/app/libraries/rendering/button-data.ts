/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {SpriteReference} from './sprite-reference';
import {TooltipInfo} from './tooltip-info';

/**
 * A wrapper object containing all the information needed to generate rendering instructions for buttons in the user interface.
 */
export class ButtonData {
  public tooltip: TooltipInfo;
  public clickFunction: () => void;
  public icon: SpriteReference;
  public iconName: string;

  constructor(tooltip: TooltipInfo, clickFunction: () => void, icon: SpriteReference, iconName: string) {
    this.tooltip = tooltip;
    this.clickFunction = clickFunction;
    this.icon = icon;
    this.iconName = iconName;
  }
}
