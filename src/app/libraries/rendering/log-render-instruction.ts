/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Point} from '../world/point';
import {RenderInstruction} from './render-instruction';
import {RenderingLayer} from './rendering-layer.enum';

export class LogRenderInstruction extends RenderInstruction {
  public messages: string[] = [];

  constructor(screenPos: Point, size: Point, color: string, messages: string[]) {
    super(screenPos, size, color);

    this.messages = messages;
    this.renderingLayer = RenderingLayer.log;
  }
}
