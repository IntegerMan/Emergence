/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../material-color.enum';
import { Point } from '../world/point';
import { RenderInstruction } from './render-instruction';
import { RenderingLayer } from './rendering-layer.enum';
import { SpriteReference } from './sprite-reference';

export class ButtonInstruction extends RenderInstruction {
  public foreColor: string = MaterialColor.white;
  public shadowColor: string = MaterialColor.greyDark4;
  public disabledColor: string = MaterialColor.greyDark2;
  public hoverColor: string = MaterialColor.tealLight1;
  public pressedColor: string = MaterialColor.tealLight3;
  public content: string = null;
  public isEnabled: boolean = true;
  public radius: number = 3;
  public alwaysRenderHovered: boolean = false;
  public icon: SpriteReference;
  public iconOpacity: number = 1;
  public iconName: string;

  constructor(screenPos: Point, size: Point, color: string) {
    super(screenPos, size, color);

    this.renderingLayer = RenderingLayer.systemButtons;
  }
}
