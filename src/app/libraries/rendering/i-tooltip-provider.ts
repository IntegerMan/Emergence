/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {RenderInstruction} from './render-instruction';

export interface ITooltipProvider {
  requestTooltip(instruction: RenderInstruction): void;
  cancelTooltip(instruction: RenderInstruction): void;
}
