/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Point} from '../world/point';
import {RenderInstruction} from './render-instruction';
import {RenderingLayer} from './rendering-layer.enum';

export class RectangleInstruction extends RenderInstruction {
  public isFilled: boolean = true;
  public alpha: number = 1;

  constructor(screenPos: Point, size: Point, bgColor: string, isFilled: boolean = true) {
    super(screenPos, size, bgColor);

    this.isFilled = isFilled;
    this.renderingLayer = RenderingLayer.level;
  }
}
