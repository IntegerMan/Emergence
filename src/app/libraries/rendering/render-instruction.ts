/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommandSlot } from '../world/actors/commands/command-slot';
import { Point } from '../world/point';
import { RenderingLayer } from './rendering-layer.enum';
import { TooltipInfo } from './tooltip-info';

export abstract class RenderInstruction {
  public bgColor: string;
  public startPixel: Point;
  public size: Point;
  public clickFunction: () => void;
  public renderingLayer: RenderingLayer;
  public slot: CommandSlot;

  /**
   * The delay of the fade effect with 0 or lower being no fade, 0.1 being a very slow fade, and 12 being a fast fade.
   * @type {number}
   */
  public fadeRate: number = 0;

  /**
   * The duration of a color pulse effect, with 0 and lower being off, 0.1 being as fast as possible, and 12 being fairly slow and steady
   * @type {number}
   */
  public pulseDuration: number = 0;

  /**
   * Information on the tooltip associated with the instruction, if any is present.
   */
  public tooltip: TooltipInfo;

  public id: string;

  public isInToolTip: boolean = false;

  protected constructor(screenPos: Point, size: Point, bgColor: string = null) {
    this.startPixel = screenPos;
    this.bgColor = bgColor;
    this.size = size;
  }
}
