/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import * as chromatism from 'chromatism';
import { ColourModes, ColourObject } from 'chromatism';
import RGB = ColourModes.RGB;

export class ColorHelper {
  private static shadeValues: string[];
  private static darkenValues: string[];

  public static getHexColorFromString(color: string): number {
    // Drop the # from it and interpret it as a number like Pixi wants
    return +`0x${color.substring(1)}`;
  }

  public static getHexColorFromRGB(rgb: RGB): number {
    const webColor: ColourModes.HEX = chromatism.convert(rgb).hex;

    return this.getHexColorFromString(webColor);
  }

  public static getRGBFromWeb(webColor: string): RGB {
    return chromatism.convert(webColor).rgb;
  }

  /**
   * Gets a darker variant of the requested shade by a specified percent.
   * This method is performance intensive, so values are cached.
   * @param {string} color the color to convert
   * @param {number} percent the percent of the color that should be changed with smaller values meaning less change
   * @returns {string} the hex string of the darkened version of color
   */
  public static darkenRGB(color: ColourModes.Any, percent: number): RGB {
    // Initialize the cache as needed
    if (!this.darkenValues) {
      this.darkenValues = [];
    }

    const inColor: ColourObject = chromatism.convert(color);

    const key: string = `${inColor.hex}_${percent}`;

    // Check to see if we have it in the cache
    if (this.darkenValues[key]) {
      return this.darkenValues[key];
    }

    const colourObject: ColourObject = chromatism.shade(percent * -100, inColor.rgb);

    // Stick it in the cache for next time
    this.darkenValues[key] = colourObject.rgb;

    // console.debug(`Changed ${inColor.hex} by ${percent} to ${colourObject.hex}`, colourObject);
    return colourObject.rgb;
  }

  /**
   * Gets a faded variant of the requested shade.
   * This method is performance intensive, so values are cached.
   * @param {string} bgColor the color to convert
   * @returns {string} the hex string of the shaded version of bgColor
   */
  public static getFadedColor(bgColor: string): string {
    // Initialize the cache as needed
    if (!this.shadeValues) {
      this.shadeValues = [];
    }

    // Check to see if we have it in the cache
    if (this.shadeValues[bgColor]) {
      return this.shadeValues[bgColor];
    }

    // We don't have it in the cache, do the computation
    let c: ColourObject = chromatism.convert(bgColor);
    c = chromatism.contrast(0.25, c.rgb);
    c = chromatism.shade(-5, c.rgb);

    // Store the value for next time
    this.shadeValues[bgColor] = c.hex;

    return c.hex;
  }
}
