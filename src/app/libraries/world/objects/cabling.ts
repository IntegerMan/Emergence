/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { BitwiseObjectHelper } from './bitwise-object-helper';
import { GameObject } from './game-object';

export class Cabling extends GameObject {
  private _spriteReference: SpriteReference;

  constructor(level: GameLevel) {
    super(level);
  }

  get name(): string {
    return 'Pipeline';
  }

  get foregroundColor(): string {
    return MaterialColor.white;
  }

  get spriteReference(): SpriteReference {
    return this._spriteReference;
  }

  get blocksSight(): boolean {
    return false;
  }

  get blocksMovement(): boolean {
    return false;
  }

  get backgroundColor(): string {
    return undefined;
  }

  get zIndex(): number {
    return 1;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    return false;
  }

  public calculateSprite(): void {
    const bits: number = BitwiseObjectHelper.calculateBits(this, (o: GameObject): boolean => o instanceof Cabling);
    this._spriteReference = new SpriteReference(SpriteSheetReference.small, bits, 10, MaterialColor.orangeAccent2, 1);
  }
}
