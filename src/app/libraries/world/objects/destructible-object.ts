/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Logger} from '../../../shared/utility/logger';
import {Cell} from '../cell';
import {GameLevel} from '../game-level';
import {GameObject} from './game-object';
import {Metadata} from './metadata';

export abstract class DestructibleObject extends GameObject {
  public currentHp: number;
  public maxHp: number;

  protected constructor(level: GameLevel, health: number) {
    super(level);

    this.currentHp = health;
    this.maxHp = health;
  }

  public getDamageResistance(): number {
    return 0;
  }

  public damage(damage: number): void {
    if (this.isDestroyed) {
      return;
    }

    this.currentHp = Math.max(0, this.currentHp - damage);

    Logger.debug(`Damaging ${this.name} at ${this.cell.pos.toString()} by ${damage}. CurrentHP is now ${this.currentHp}`);

    if (this.isDestroyed) {
      this.onDestroyed();
    }
  }

  get isDestroyed(): boolean {
    return this.currentHp <= 0;
  }

  protected onDestroyed(): void {
    // Grab a ref to the cell since we're about to leave it
    const cell: Cell = this.cell;

    this.level.removeObject(this);

    // Spawn metadata (the game's equivalent of rubble)
    if (cell.objects.filter((o: GameObject): boolean => o instanceof Metadata).length <= 0) {
      this.level.addObject(new Metadata(this.level), cell.pos);
    }
  }
}
