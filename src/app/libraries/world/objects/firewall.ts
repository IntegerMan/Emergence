/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { GameObject } from './game-object';

export class Firewall extends GameObject {
  constructor(level: GameLevel) {
    super(level);
  }

  get isOpen(): boolean {
    return this.level.hasAdminAccess;
  }

  get name(): string {
    return 'Firewall';
  }

  get blocksSight(): boolean {
    return false;
  }

  get blocksMovement(): boolean {
    return !this.isOpen;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    if (this.blocksMovement) {
      context.output(`You need admin access on this machine before you can pass through the firewall.`);

      return true;
    }

    return false;
  }

  get spriteReference(): SpriteReference {
    return this.isOpen
      ? new SpriteReference(SpriteSheetReference.small, 1, 0, '', 0.4)
      : new SpriteReference(SpriteSheetReference.small, 2, 0, '', 0.6);
  }

  get foregroundColor(): string {
    if (this.isOpen) {
      return MaterialColor.teal;
    } else {
      return MaterialColor.deepOrange;
    }
  }
}
