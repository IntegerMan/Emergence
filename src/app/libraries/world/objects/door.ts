/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { DestructibleObject } from './destructible-object';

export class Door extends DestructibleObject {
  private isOpen: boolean;
  private isEastWest: boolean;

  constructor(level: GameLevel, isOpen: boolean = false, isEastWest: boolean = false) {
    super(level, 2);

    this.isOpen = isOpen;
    this.isEastWest = isEastWest;
  }

  get name(): string {
    if (this.isOpen) {
      return 'Open Access Port';
    } else {
      return 'Closed Access Port';
    }
  }

  get spriteReference(): SpriteReference {
    if (this.isEastWest) {
      if (this.isOpen) {
        return new SpriteReference(SpriteSheetReference.small, 3, 8);
      } else {
        return new SpriteReference(SpriteSheetReference.small, 2, 8);
      }
    } else {
      if (this.isOpen) {
        return new SpriteReference(SpriteSheetReference.small, 1, 8);
      } else {
        return new SpriteReference(SpriteSheetReference.small, 0, 8);
      }
    }
  }

  get blocksSight(): boolean {
    return !this.isOpen;
  }

  get blocksMovement(): boolean {
    return !this.isOpen;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    // Open the door but don't go through it
    if (!this.isOpen) {
      this.isOpen = true;

      return true;
    }

    return false;
  }

  get foregroundColor(): string {
    return MaterialColor.yellow;
  }

  get backgroundColor(): string {
    return undefined;
  }
}
