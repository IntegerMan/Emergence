/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
/**
 * Represents an object that can be picked up by the player (and optionally potentially dropped later, depending on the item type)
 */
import { GameObject } from './game-object';

export abstract class Pickup extends GameObject {

  get foregroundColor(): string {
    return MaterialColor.white;
  }

  get blocksSight(): boolean {
    return false;
  }

  get blocksMovement(): boolean {
    return false;
  }
}
