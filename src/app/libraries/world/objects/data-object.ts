/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { DestructibleObject } from './destructible-object';

export class DataObject extends DestructibleObject {
  constructor(level: GameLevel) {
    super(level, 2);
  }

  get name(): string {
    return 'Data Store';
  }

  get blocksMovement(): boolean {
    return true;
  }

  get foregroundColor(): string {
    return MaterialColor.amber;
  }

  get blocksSight(): boolean {
    return false;
  }

  get spriteReference(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.small, 4, 0);
  }

  get backgroundColor(): string {
    return undefined;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    context.output(`The data store doesn't contain anything of interest.`);

    return this.blocksMovement;
  }
}
