/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { BitwiseObjectHelper } from './bitwise-object-helper';
import { GameObject } from './game-object';
import { Partition } from './partition';

export class ThreadPool extends GameObject {
  private _spriteReference: SpriteReference;

  constructor(level: GameLevel) {
    super(level);
  }

  get name(): string {
    return 'Thread Pool';
  }

  get spriteReference(): SpriteReference {
    return this._spriteReference;
  }

  get foregroundColor(): string {
    return MaterialColor.blueLight2;
  }

  get blocksMovement(): boolean {
    return true;
  }

  get blocksSight(): boolean {
    return false;
  }

  get backgroundColor(): string {
    return MaterialColor.blueDark4;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    context.output(`You can't go in the thread pool - nobody implemented swimming!`);

    return this.blocksMovement;
  }

  public calculateSprite(): void {
    const bits: number = BitwiseObjectHelper.calculateBits(
      this,
      (o: GameObject): boolean => o instanceof ThreadPool || o instanceof Partition
    );
    this._spriteReference = new SpriteReference(SpriteSheetReference.small, bits, 12);
  }
}
