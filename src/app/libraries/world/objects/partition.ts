/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { BitwiseObjectHelper } from './bitwise-object-helper';
import { DestructibleObject } from './destructible-object';
import { GameObject } from './game-object';

export class Partition extends DestructibleObject {
  public isExternal: boolean = false;

  private _spriteReference: SpriteReference;

  constructor(level: GameLevel, isExternal: boolean) {
    super(level, 3);
    this.isExternal = isExternal;
  }

  public damage(damage: number): void {
    // Only allow damage if we're not an external wall
    if (!this.isExternal) {
      super.damage(damage);
    }
  }

  get name(): string {
    return this.isExternal ? `External Partition` : 'Partition';
  }

  get foregroundColor(): string {
    return this.level.hasAdminAccess ? MaterialColor.greenAccent2 : this.currentHp === 1 ? MaterialColor.grey : MaterialColor.white;
  }

  get spriteReference(): SpriteReference {
    return this._spriteReference;
  }

  get blocksSight(): boolean {
    return true;
  }

  get blocksMovement(): boolean {
    return true;
  }

  get backgroundColor(): string {
    return undefined;
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    context.output(`You can't breach the ${this.name}.`);

    return this.blocksMovement;
  }

  public calculateSprite(): void {
    const bits: number = BitwiseObjectHelper.calculateBits(this, (o: GameObject): boolean => o instanceof Partition);
    this._spriteReference = new SpriteReference(SpriteSheetReference.small, bits, 11);
  }
}
