/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { DestructibleObject } from './destructible-object';

export class ServiceObject extends DestructibleObject {
  public isActive: boolean = true;

  constructor(level: GameLevel, isActive: boolean = true) {
    super(level, 2);

    this.isActive = isActive;
  }

  get name(): string {
    return 'Service';
  }

  get foregroundColor(): string {
    return MaterialColor.teal;
  }

  get blocksSight(): boolean {
    return true;
  }

  get blocksMovement(): boolean {
    return true;
  }

  get backgroundColor(): string {
    return undefined;
  }

  get spriteReference(): SpriteReference {
    return this.isActive ? new SpriteReference(SpriteSheetReference.small, 4, 5) : new SpriteReference(SpriteSheetReference.small, 6, 5);
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    if (this.isActive) {
      context.output('The service spins silently as it performs its tasks.');
    } else {
      context.output('The service lays dormant, apparently inactive.');
    }

    return this.blocksMovement;
  }
}
