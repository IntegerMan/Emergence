/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { MaterialColor } from '../../material-color.enum';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from '../actors/actor';
import { CommandContext } from '../actors/commands/command-context';
import { GameLevel } from '../game-level';
import { GameObject } from './game-object';

export class NetworkPort extends GameObject {
  private readonly isIncoming: boolean;

  constructor(level: GameLevel, isIncoming: boolean) {
    super(level);

    this.isIncoming = isIncoming;
  }

  get name(): string {
    if (this.isIncoming) {
      return 'Inbound Network Port';
    } else {
      return 'Outbound Network Port';
    }
  }

  get blocksSight(): boolean {
    return false;
  }

  get blocksMovement(): boolean {
    return this.isIncoming;
  }

  get spriteReference(): SpriteReference {
    return this.isIncoming ? new SpriteReference(SpriteSheetReference.small, 1, 1) : new SpriteReference(SpriteSheetReference.small, 2, 1);
  }

  public handleInteraction(actor: Actor, context: CommandContext): boolean {
    if (this.isIncoming) {
      context.output(`You need to go through the outbound network port, not the inbound one.`);

      return true;
    } else {
      // Woo hoo! We've completed the level!
      this.level.isCompleted = true;
    }

    return false;
  }

  get foregroundColor(): string {
    return MaterialColor.yellow;
  }
}
