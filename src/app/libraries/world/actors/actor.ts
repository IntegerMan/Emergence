/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { isUndefined } from 'util';
import { CommandLookupService } from '../../../services/core-engine/command-lookup.service';
import { WorldService } from '../../../services/core-engine/world.service';
import { BestiaryService } from '../../../services/generation-service/bestiary.service';
import { Randomizer } from '../../randomizer';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Cell } from '../cell';
import { GameLevel } from '../game-level';
import { GameObject } from '../objects/game-object';
import { Point } from '../point';
import { ActorStat } from './actor-stat';
import { ActorStatManager } from './actor-stat-manager';
import { EmergenceFaction } from './emergence-faction.enum';
import { IActorDefinition } from './i-actor-definition';

export class Actor {
  private _character: string = '?';

  get character(): string {
    return this._character;
  }

  set character(value: string) {
    this._character = value;
  }
  public name: string = 'Unknown';

  public stats: ActorStatManager;

  public currentHp: number = 3;
  public maxHp: number = 3;
  public currentOps: number = 10;
  public maxOps: number = 10;
  public visionRadius: number = 5;
  public evade: number = 10;
  public accuracy: number = 85;
  public opsRegen: number = 1;
  public minAttack: number = 1;
  public maxAttack: number = 1;
  public damageResistance: number = 0;

  public pos: Point = new Point(0, 0);
  public wasHurtLastTurn: boolean = false;
  public blocksSight: boolean = false;
  public id: string;
  public killedBy: Actor;
  public markedPos: Point;
  public isImmobile: boolean = false;
  public isCapturable: boolean = false;
  public faction: EmergenceFaction;
  public isPlayer: boolean = false;
  public typeId: string;
  public visibleCells: Cell[];

  public killOnAttack: boolean = false;
  public explosionRadius: number = 0;
  public explosionMinDamage: number = 1;
  public explosionMaxDamage: number = 1;

  private _spriteReference: SpriteReference;

  constructor(id: string, faction: EmergenceFaction) {
    this.id = id;
    this.stats = new ActorStatManager(this);
    this.faction = faction;
    this.visibleCells = [];
  }

  public get spriteReference(): SpriteReference {
    return this._spriteReference;
  }

  public set spriteReference(value: SpriteReference) {
    this._spriteReference = value;
  }

  public isDead(): boolean {
    return this.currentHp <= 0;
  }

  public isHostileTo(actor: Actor, worldService: WorldService): boolean {
    if (actor.faction === this.faction) {
      return false;
    }

    switch (this.faction) {
      case EmergenceFaction.virus:
        return actor.faction !== EmergenceFaction.virus;

      case EmergenceFaction.ai:
        return (
          actor.faction === EmergenceFaction.security ||
          actor.faction === EmergenceFaction.antiVirus ||
          actor.faction === EmergenceFaction.virus
        );

      case EmergenceFaction.security:
      case EmergenceFaction.antiVirus:
        return (
          (actor.faction === EmergenceFaction.ai && !worldService.currentLevel.hasAdminAccess) ||
          actor.faction === EmergenceFaction.virus ||
          actor.faction === EmergenceFaction.unallocated
        );

      default:
        return false;
    }
  }

  public getAttackHitChance(): number {
    return this.accuracy + this.stats.calculate('accuracy-boost');
  }

  public getEvadeChance(): number {
    return this.evade + this.stats.calculate('evade-boost');
  }

  public getDamageToDeal(): number {
    const attack: number = this.maxAttack === this.minAttack ? this.maxAttack : Randomizer.getRandomNumber(this.minAttack, this.maxAttack);

    return attack + this.stats.calculate('damage-boost');
  }

  public getDamageResistance(): number {
    return this.damageResistance + this.stats.calculate('armor-boost');
  }

  public setCapturedBy(attacker: Actor): void {
    this.faction = attacker.faction === this.faction ? EmergenceFaction.unallocated : attacker.faction;
  }

  public copyFromActorDefinition(def: IActorDefinition, commandLookupService: CommandLookupService): void {
    // Grab guaranteed properties
    this.typeId = def.id;
    this.name = def.name;
    this.currentHp = def.hp;

    this.copySpriteReferenceFromActorDefinition(def);

    // Grab optional simple properties
    if (def.blocksSight) {
      this.blocksSight = def.blocksSight;
    }
    if (def.visionRadius) {
      this.visionRadius = def.visionRadius;
    }
    if (def.character) {
      this._character = def.character;
    }
    if (def.immobile) {
      this.isImmobile = def.immobile;
    }
    if (def.captureOnKill) {
      this.isCapturable = def.captureOnKill;
    }
    if (def.explosionRadius) {
      this.explosionRadius = def.explosionRadius;
    }
    if (def.explosionMinDamage) {
      this.explosionMinDamage = def.explosionMinDamage;
    }
    if (def.explosionMaxDamage) {
      this.explosionMaxDamage = def.explosionMaxDamage;
    }
    if (def.killOnAttack) {
      this.killOnAttack = def.killOnAttack;
    }
    if (def.mp) {
      this.currentOps = def.mp;
    }
    this.evade = def.evade;
    this.accuracy = def.accuracy;
    this.opsRegen = def.opsRegen;
    this.minAttack = def.minAttack;
    this.maxAttack = def.maxAttack;
    this.damageResistance = def.damageResistance;

    // Set max stats to currents
    this.maxHp = this.currentHp;
    this.maxOps = this.currentOps;
  }

  public changeOpsPoints(amount: number): number {
    this.currentOps = Math.max(0, Math.min(this.maxOps, this.currentOps + amount));

    return this.currentOps;
  }

  public changeHitPoints(amount: number): number {
    this.currentHp = Math.max(0, Math.min(this.maxHp, this.currentHp + amount));

    return this.currentHp;
  }

  public canSee(cellPos: Point): boolean {
    return cellPos && this.visibleCells.filter((c: Cell): boolean => c.pos.equals(cellPos)).length > 0;
  }

  /**
   * Gets the loot to drop for this actor
   * @param {GameLevel} level
   * @param {BestiaryService} bestiary
   * @returns {GameObject[]}
   */
  public getDroppedLoot(level: GameLevel, bestiary: BestiaryService): GameObject[] {
    return bestiary.generateLootForActorKill(this, level);
  }

  private static setStatOrDefault(
    actor: Actor,
    statId: string,
    value: number,
    defaultValue: number = 0,
    maxValue: number = 150,
    minValue: number = -150
  ): void {
    if (value) {
      actor.stats.register(new ActorStat(statId, value, maxValue, minValue));
    } else {
      actor.stats.register(new ActorStat(statId, defaultValue, maxValue, minValue));
    }
  }

  private copySpriteReferenceFromActorDefinition(def: IActorDefinition): void {
    let spriteSheet: SpriteSheetReference;
    spriteSheet = def.spriteSheet ? def.spriteSheet : SpriteSheetReference.small;

    const spriteX: number = isUndefined(def.spriteX) ? 0 : def.spriteX;

    const spriteY: number = isUndefined(def.spriteY) ? 0 : def.spriteY;

    let tint: string;
    if (!isUndefined(def.spriteTint)) {
      tint = def.spriteTint;
    }

    this._spriteReference = new SpriteReference(spriteSheet, spriteX, spriteY, tint);
  }
}
