/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { ActiveCommand } from './active-command';
import { CommandClassificationType } from './command-classification-type.enum';

export class TargetingCommand extends ActiveCommand {
  constructor() {
    super('targeting', 'TARGET', 'Targeting', 'Increases your accuracy and makes you less likely to miss.', 1);

    this.modifiesStat('accuracy-boost', 10);
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 4, 14);
  }

  public get iconName(): string {
    return 'gps_fixed';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.utility;
  }

}
