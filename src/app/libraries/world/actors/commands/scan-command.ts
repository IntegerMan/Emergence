/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { ActiveCommand } from './active-command';
import { CommandClassificationType } from './command-classification-type.enum';

export class ScanCommand extends ActiveCommand {
  constructor() {
    super('scan', 'SCAN', 'Scan', 'Extends your visible range and helps identify hidden objects.', 1);

    this.modifiesStat('vision-boost', 3);
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 3, 14);
  }

  public get iconName(): string {
    return 'perm_scan_wifi';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.utility;
  }

}
