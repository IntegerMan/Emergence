/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Cell } from '../../cell';
import { ActorCommand } from './actor-command';
import { CommandContext } from './command-context';

export abstract class TargetedCommand extends ActorCommand {
  public target: Cell = null;
  public executionContext: CommandContext;
  public maxRange: number = 42;

  protected constructor(id: string, label: string, name: string, description: string, activationCost: number) {
    super(id, label, name, description, activationCost);
  }

  public getTargetMessage(context: CommandContext): string {
    return `Select a target for ${this.name}`;
  }

  public checkValidTarget(cell: Cell): string {
    if (this.maxRange && cell.pos.calculateDistanceFrom(this.executionContext.invoker.pos) > this.maxRange) {
      return `This target is out of range.`;
    }

    return null;
  }
}
