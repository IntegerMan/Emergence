/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ActiveCommand } from './active-command';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';

export class RestoreCommand extends ActiveCommand {
  constructor() {
    super('restore', 'RESTR', 'Restore', 'Gradually restores stability over time while active.', 1);
  }

  public update(context: CommandContext): void {
    super.update(context);

    // Add a trickle of passive recovery
    const hp: number = context.invoker.currentHp + 1;
    context.invoker.currentHp = Math.max(0, Math.min(hp, context.invoker.maxHp));
  }

  public get iconName(): string {
    return 'autorenew';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.restorative;
  }

}
