/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { ActiveCommand } from './active-command';
import { CommandClassificationType } from './command-classification-type.enum';

export class ArmorCommand extends ActiveCommand {
  constructor() {
    super('armor', 'ARMOR', 'Armor', 'Increases your damage resistance while active.', 1);

    this.modifiesStat('armor-boost', 1);
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 7, 14);
  }

  public get iconName(): string {
    return 'beenhere';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.defensive;
  }
}
