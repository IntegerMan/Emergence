/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { ActorCommand } from './actor-command';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';

export class StabilizeCommand extends ActorCommand {
  constructor() {
    super(
      'stabilize',
      'RESTORE',
      'Stabilize',
      'Stabilizes the process and instantly restores a large portion of stability, but at a high operational cost.',
      5
    );
  }

  protected onInvoked(context: CommandContext): boolean {
    const initialStability: number = context.invoker.currentHp;

    const restoreAmount: number = 10;

    context.invoker.currentHp = Math.min(context.invoker.currentHp + restoreAmount, context.invoker.maxHp);
    if (initialStability !== context.invoker.currentHp) {
      context.output(`${context.invoker.name} stabilizes, restoring ${context.invoker.currentHp - initialStability} stability.`);
    } else {
      context.output(`${context.invoker.name} stabilizes, remaining at full stability.`);
    }

    return true;
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 10, 14);
  }

  public get iconName(): string {
    return 'build';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.restorative;
  }
}
