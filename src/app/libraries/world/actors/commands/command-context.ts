/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {CombatService} from '../../../../services/core-engine/combat.service';
import {CommandService} from '../../../../services/core-engine/command.service';
import {WorldService} from '../../../../services/core-engine/world.service';
import {BestiaryService} from '../../../../services/generation-service/bestiary.service';
import {DialogService} from '../../../../services/ui-service/dialog.service';
import {MessageService} from '../../../../services/ui-service/message.service';
import {GameLevel} from '../../game-level';
import {Actor} from '../actor';

export class CommandContext {
  public invoker: Actor;

  public messageService: MessageService;
  public worldService: WorldService;
  public commandService: CommandService;
  public bestiaryService: BestiaryService;
  public dialogService: DialogService;
  public combatService: CombatService;

  public get level(): GameLevel {
    return this.worldService.currentLevel;
  }

  public isPlayerAction: boolean;

  public output(message: string): void {
    this.messageService.showMessage(message);
  }

  public cloneWithNewInvoker(invoker: Actor): CommandContext {
    const context: CommandContext = new CommandContext();
    context.invoker = invoker;

    context.messageService = this.messageService;
    context.commandService = this.commandService;
    context.worldService = this.worldService;
    context.bestiaryService = this.bestiaryService;
    context.dialogService = this.dialogService;
    context.combatService = this.combatService;

    context.isPlayerAction = invoker.isPlayer;

    return context;
  }
}
