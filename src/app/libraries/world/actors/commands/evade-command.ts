/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ActiveCommand } from './active-command';
import { CommandClassificationType } from './command-classification-type.enum';

export class EvadeCommand extends ActiveCommand {
  constructor() {
    super('evade', 'EVADE', 'Evade', 'Increases your evasion and makes it harder for others to hit you.', 1);

    this.modifiesStat('evade-boost', 20);
  }

  public get iconName(): string {
    return 'gps_off';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.defensive;
  }

}
