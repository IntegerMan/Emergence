/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { GoogleAnalyticsHelper } from '../../../../shared/utility/google-analytics-helper';
import { Logger } from '../../../../shared/utility/logger';
import { SpriteReference } from '../../../rendering/sprite-reference';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';

export abstract class ActorCommand {
  public id: string;
  public label: string;
  public name: string;
  public activationCost: number;
  public description: string;

  protected constructor(id: string, label: string, name: string, description: string, activationCost: number) {
    this.id = id.toLowerCase();
    this.label = label;
    this.name = name;
    this.description = description;
    this.activationCost = activationCost;
  }

  public abstract get commandType(): CommandClassificationType;

  public invoke(context: CommandContext): boolean {
    this.logInvoke(context);

    return this.onInvoked(context);
  }

  public get icon(): SpriteReference {
    return undefined;
  }

  protected logInvoke(context: CommandContext): void {
    Logger.debug(`${this.name} command invoked`, context);
    if (context.isPlayerAction) {
      GoogleAnalyticsHelper.emitEvent('Command', 'Invoke', this.name);
    }
  }

  protected onInvoked(context: CommandContext): boolean {
    context.output(`${context.invoker.name} uses ${this.name} but this is not yet implemented.`);

    return true;
  }

  public get activationCostText(): string {
    return this.activationCost === 1 ? `1 Op` : `${this.activationCost} Ops`;
  }

  /**
   * Gets the name of the material icon to use for displaying this command in HTML views.
   * See http://materializecss.com/icons.html for a full list.
   * @returns {string}
   */
  public abstract get iconName(): string;
}
