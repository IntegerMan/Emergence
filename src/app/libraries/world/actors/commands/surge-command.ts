/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { SpriteReference } from '../../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../../rendering/sprite-sheet-reference.enum';
import { Cell } from '../../cell';
import { CommandClassificationType } from './command-classification-type.enum';
import { CommandContext } from './command-context';
import { TargetedCommand } from './targeted-command';

export class SurgeCommand extends TargetedCommand {
  constructor() {
    super('surge', 'SURGE', 'Surge', `Triggers a surge at the target location, causing damage to the stability of nearby processes.`, 4);

    this.maxRange = 5.5;
  }

  public checkValidTarget(cell: Cell): string {
    const superResult: string = super.checkValidTarget(cell);
    if (superResult) {
      return superResult;
    }

    if (!this.executionContext.invoker.canSee(cell.pos)) {
      return `You need a direct line of sight to use ${this.name}.`;
    }

    return super.checkValidTarget(cell);
  }

  protected onInvoked(context: CommandContext): boolean {
    context.output(`${context.invoker.name} uses ${this.name}.`);
    const bonus: number = context.invoker.stats.calculate('damage-boost');

    context.combatService.handleExplosion(context, this.target.pos, 2, bonus + 1, bonus + 3);

    return true;
  }

  public get icon(): SpriteReference {
    return new SpriteReference(SpriteSheetReference.scifi, 5, 14);
  }

  public get iconName(): string {
    return 'flash_on';
  }

  public get commandType(): CommandClassificationType {
    return CommandClassificationType.offensive;
  }

}
