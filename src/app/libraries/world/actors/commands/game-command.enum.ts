/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export enum GameCommand {
  internalCommand = 'INTERNAL_COMMAND',
  showInventory = 'SHOW_INVENTORY',
  cancel = 'CANCEL',
  toggleDebugMode = 'DEBUG',
  move = 'MOVE',
  wait = 'WAIT',
  zoomIn = 'ZOOM_IN',
  zoomOut = 'ZOOM_OUT'
}
