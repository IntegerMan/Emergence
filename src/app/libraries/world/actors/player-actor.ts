/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {isNullOrUndefined} from 'util';
import {CommandLookupService} from '../../../services/core-engine/command-lookup.service';
import {BestiaryService} from '../../../services/generation-service/bestiary.service';
import {Logger} from '../../../shared/utility/logger';
import {GameLevel} from '../game-level';
import {CommandPickup} from '../objects/command-pickup';
import {GameObject} from '../objects/game-object';
import {Actor} from './actor';
import {ActorCommand} from './commands/actor-command';
import {CommandSlot} from './commands/command-slot';
import {EmergenceFaction} from './emergence-faction.enum';
import {IActorDefinition} from './i-actor-definition';

export class PlayerActor extends Actor {
  public storedCommandSlots: CommandSlot[];
  public installedCommandSlots: CommandSlot[];

  private _hasAdminAccess: boolean = false;

  constructor(id: string, faction: EmergenceFaction) {
    super(id, faction);

    this.storedCommandSlots = [];
    this.installedCommandSlots = [];
  }

  public get commands(): ActorCommand[] {
    return this.installedCommandSlots
      .filter((s: CommandSlot): boolean => !isNullOrUndefined(s.command))
      .map((s: CommandSlot): ActorCommand => s.command);
  }

  public getCommand(index: number): ActorCommand {
    const slot: CommandSlot = this.installedCommandSlots[index] ? this.installedCommandSlots[index] : null;

    return slot ? slot.command : null;
  }

  public addCommand(command: ActorCommand, isInstalled: boolean = false): void {
    const slot: CommandSlot = new CommandSlot(isInstalled, command);

    if (isInstalled) {
      this.installedCommandSlots.push(slot);
    } else {
      this.storedCommandSlots.push(slot);
    }
  }

  public finalizeCommands(): void {
    while (this.storedCommandSlots.length < 30) {
      this.storedCommandSlots.push(new CommandSlot(false));
    }

    while (this.installedCommandSlots.length < 10) {
      this.installedCommandSlots.push(new CommandSlot(true));
    }

    let i: number = 1;
    for (const slot of this.installedCommandSlots) {
      if (i === 10) {
        slot.shortcutKey = '0';
      } else {
        slot.shortcutKey = String(i++);
      }
    }

    for (const slot of this.storedCommandSlots) {
      slot.shortcutKey = undefined;
    }
  }

  public findEmptyCommandSlot(): CommandSlot {
    const candidates: CommandSlot[] = [];
    candidates.push(...this.installedCommandSlots.filter((s: CommandSlot): boolean => !s.command));
    candidates.push(...this.storedCommandSlots.filter((s: CommandSlot): boolean => !s.command));

    if (candidates.length > 0) {
      return candidates[0];
    }
  }

  public getDroppedLoot(level: GameLevel, bestiary: BestiaryService): GameObject[] {
    const loot: GameObject[] = [];

    for (const slot of this.storedCommandSlots.filter((s: CommandSlot): boolean => !isNullOrUndefined(s.command))) {
      loot.push(new CommandPickup(level, slot.command));
    }
    for (const slot of this.installedCommandSlots.filter((s: CommandSlot): boolean => !isNullOrUndefined(s.command))) {
      loot.push(new CommandPickup(level, slot.command));
    }

    return loot;
  }

  public copyFromActorDefinition(def: IActorDefinition, commandLookupService: CommandLookupService): void {
    super.copyFromActorDefinition(def, commandLookupService);

    if (def.commands) {
      for (const command of def.commands) {
        const emptySlot: CommandSlot = this.installedCommandSlots.find((c: CommandSlot): boolean => isNullOrUndefined(c.command));
        const actualCommand: ActorCommand = commandLookupService.findCommand(command);

        if (actualCommand) {
          this.addCommand(actualCommand, isNullOrUndefined(emptySlot));
        } else {
          Logger.warn(`Could not find command ${command}`);
        }
      }
    }
  }

  public get hasAdminAccess(): boolean {
    return this._hasAdminAccess;
  }

  public set hasAdminAccess(value: boolean) {
    this._hasAdminAccess = value;
  }
}
