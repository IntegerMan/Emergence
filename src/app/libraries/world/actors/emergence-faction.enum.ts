/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export enum EmergenceFaction {
  virus = 'virus',
  unallocated = 'unallocated',
  ai = 'ai',
  system = 'sys',
  security = 'sys-sec',
  antiVirus = 'anti-virus'
}
