/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Randomizer } from '../../randomizer';
import { SpriteReference } from '../../rendering/sprite-reference';
import { SpriteSheetReference } from '../../rendering/sprite-sheet-reference.enum';
import { Actor } from './actor';
import { EmergenceFaction } from './emergence-faction.enum';

export class BitActor extends Actor {
  public isSet: boolean = false;

  constructor(id: string, faction: EmergenceFaction) {
    super(id, faction);

    this.isSet = Randomizer.getRandomBoolean();
  }

  get character(): string {
    if (this.isSet) {
      return '1';
    } else {
      return '0';
    }
  }

  get spriteReference(): SpriteReference {
    if (this.isSet) {
      return new SpriteReference(SpriteSheetReference.small, 14, 4);
    } else {
      return new SpriteReference(SpriteSheetReference.small, 13, 4);
    }
  }
}
