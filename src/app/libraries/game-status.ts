/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export enum GameStatus {
  gameSetup = 'game_setup',
  standard = 'standard',
  selectingTarget = 'target_selection',
  gameOver = 'game_over'
}
