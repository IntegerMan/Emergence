import { TestBed, inject } from '@angular/core/testing';

import { ReleaseNotesService } from './release-notes.service';

describe('ReleaseNotesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReleaseNotesService]
    });
  });

  it('should be created', inject([ReleaseNotesService], (service: ReleaseNotesService) => {
    expect(service).toBeTruthy();
  }));
});
