/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { GameLoopService } from '../../services/core-engine/game-loop.service';
import { HelpService } from '../help.service';
import { IHelpCategory } from '../i-help-category';
import { IHelpTopic } from '../i-help-topic';

@Component({
  selector: 'em-help-screen',
  templateUrl: './help-screen.component.html',
  styleUrls: ['./help-screen.component.scss']
})
export class HelpScreenComponent implements OnInit, OnDestroy {
  public isInGame: boolean;
  public categories: IHelpCategory[];
  public selectedCategory: IHelpCategory;
  private sub: Subscription;

  constructor(private gameService: GameLoopService, private helpService: HelpService) {}

  public ngOnInit(): void {
    this.isInGame = this.gameService.isInGame;
    this.categories = this.helpService.categories;
    this.selectedCategory = this.helpService.selectedCategory;
    this.sub = this.helpService.selectedCategoryChanged.subscribe((c: IHelpCategory) => (this.selectedCategory = c));
  }

  public ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  public onCategoryOpened(category: IHelpCategory): void {
    this.selectedCategory = category;
  }

  public getRoute(category: IHelpCategory | IHelpTopic): string[] {
    return HelpService.getRouterLink(category);
  }
}
