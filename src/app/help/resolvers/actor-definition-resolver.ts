/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { IActorDefinition } from '../../libraries/world/actors/i-actor-definition';
import { ActorDefinitionService } from '../../services/generation-service/actor-definition.service';
import { Logger } from '../../shared/utility/logger';

@Injectable()
export class ActorDefinitionResolver implements Resolve<IActorDefinition> {
  constructor(private actorDefinitionService: ActorDefinitionService, private router: Router) {}

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<IActorDefinition> | Promise<IActorDefinition> | IActorDefinition {
    const actorId: string = route.paramMap.get('classId').toLowerCase();

    // Grab the topic out of the category. This COULD just call helpService.findTopic, but this wouldn't restrict it to the category
    const actor: IActorDefinition = this.actorDefinitionService.getActorDefinition(actorId);

    if (!actor) {
      this.router.navigate(['/help', 'not-found']);
    }

    Logger.debug(`ActorDefinition note resolver identified actor ${actorId}`, actor);

    return actor;
  }
}
