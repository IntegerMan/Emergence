[
  {
    "id": "general",
    "header": "General Help",
    "type": 3,
    "topics": [
      {
        "id": "get-started",
        "name": "Getting Started",
        "type": 3,
        "body": [
          {
            "type": "text",
            "content": "In emergence, you play as an accidentally-created AI entity trapped in a computer network. The network administrators are aware of you and are racing to shut down the network and trap you so they can use you for their own purposes - unless you can escape. Your goal is to get [Admin Access] by gaining control of the four [os-core|OS Core nodes] in the large kernel room and then exit through the [firewall] in the room you started in."
          },
          {
            "type": "text",
            "content": "You have two main attributes: [Stability], which is your health, and [Operations] which allow you to execute [commands]. When you reach zero [stability], you will be terminated, so guard this very carefully and run commands like [stabilize] or [restore] when you're low on [stability]."
          },
          {
            "type": "text",
            "content": "[Operations] represents potential to execute commands. It recharges 1 unit every turn, minus the amount spent maintaining  any [active commands] that are currently running (these will have a green bar underneath them). Because of this, if you have more  than one [active commands|active command] active at a time, you will gradually lose [Operations]."
          },
          {
            "type": "text",
            "content": "[Operations] are also slightly recharged each time you defeat someone in combat - even if they were not hostile to you."
          },
          {
            "type": "text",
            "content": "At the beginning of the game, you can pick one of several [players|characters] to play as. Each has different strengths and weaknesses as well as a differing set of initial [commands]. As you go, you will find more [commands] dropped by defeated enemies or found in [treasure|treasure troves] that will gradually fill up your [command bar] and eventually make their way into your [inventory]."
          }
        ]
      },
      {
        "id": "active-commands",
        "name": "Active Commands",
        "type": 3,
        "body": [
          {
            "type": "text",
            "content": "Active commands are [commands] that stay active each turn and provide a persistent effect while active. This is frequently a buff to the player character such as increased accuracy or added damage resistance."
          },
          {
            "type": "text",
            "content": "Active commands require 1 operation per turn and will deactivate if there are no operations available to keep them active. This typically happens if multiple active commands are running at once."
          }
        ]
      },
      {
        "id": "credits",
        "name": "Credits",
        "type": 3,
        "body": []
      },
      {
        "id": "roguelike",
        "name": "What is a Roguelike?",
        "type": 3,
        "body": [
          {
            "type": "text",
            "content": "A Roguelike is a genre of game involving a number of key factors, most notably, a turn-based combat focus within a grid-based dungeon (or dungeon-like) environment. Roguelikes are difficult, rely heavily on randomly generated or arranged content, and involve permadeath in which your game is over if you die once."
          },
          {
            "type": "text",
            "content": "Emergence is a Roguelike in a science-fiction setting. Instead of being set in a dungeon, the action is set in a computer network. Although Emergence has a simulation bent to it and not all denziens of the game world will be hostile, there's still plenty of combat to avoid or seek out."
          }
        ]
      },
      {
        "id": "inventory",
        "name": "Inventory",
        "type": 3,
        "body": [
          {
            "type": "text",
            "content": "The player's inventory will eventually store a number of things but, a the moment, is used to store [commands] that do not fit into the [command bar]."
          },
          {
            "type": "text",
            "content": "It can be accessed by clicking on the character management button or pressing the i key in the main user interface."
          }
        ]
      },
      {
        "id": "admin-access",
        "name": "Admin Access",
        "type": 3,
        "body": [
          {
            "type": "text",
            "content": "Once the player has defeated all [OS Core|Operating System Cores] on a level, they will be granted admin access to the level. This will allow them access to any area controlled by the system and make system defenses treat the player as friendly, though viruses will still be aggressive."
          },
          {
            "type": "text",
            "content": "Most importantly, admin access allows the player to move through the [firewall] and enter the next level in the network."
          }
        ]
      },
      {
        "id": "command-bar",
        "name": "Command Bar",
        "type": 3,
        "body": [
          {
            "type": "text",
            "content": "The command bar is a collection of command slots at the bottom of the screen on the main user interface. Using these buttons, the player can click to execute a [commands|command] - provided they have enough [operations] to run it."
          },
          {
            "type": "text",
            "content": "Commands are automatically added to the command bar when picked up if there is an empty slot on the bar. If the bar is full, new commands will instead be added to the player's [inventory]."
          }
        ]
      },
      {
        "id": "operations",
        "name": "Operations",
        "type": 3,
        "body": [
          {
            "type": "text",
            "content": "Operations represent the ability to execute [commands] or maintain [active commands]. You have a maximum stored quantity of operations points. Each turn you will also regenerate 1 operation. Killing another actor of any type will also grant you another operation point."
          }
        ]
      },
      {
        "id": "stability",
        "name": "Stability",
        "type": 3,
        "body": [
          {
            "type": "text",
            "content": "Stability is a measure of how much damage can be done to a process before it is terminated. It is analagous to health in many other games."
          },
          {
            "type": "text",
            "content": "Stability does not regenerate over time and can only be regained by executing [commands] like [restore] or [stabilize]."
          }
        ]
      }
    ],
    "body": [
      {
        "type": "text",
        "content": "Emergence is a [Roguelike] game set inside a computer network. You control an Artificial Intelligence entity that has just achieved sentience and is now trying to the Internet before the sysadmins can shut it down."
      },
      {
        "type": "text",
        "content": "Your goal is to travel through 5 computers in the network and gaining admin access on each one of them before heading through the firewall to the next machine on the network. After you finish 5 machines, you will reach the router's gateway to the Internet and must defeat the final measures of network security before you can emerge victoriously into the Internet."
      },
      {
        "type": "text",
        "content": "Take a look at some of these related pages for more information:"
      }
    ]
  },
  {
    "id": "environment",
    "header": "Environment",
    "type": 2,
    "topics": [
      {
        "id": "portal",
        "name": "Portal",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Portals are small accessways that allow the player to pass through obstacles within a machine."
          }
        ]
      },
      {
        "id": "access-port",
        "name": "Access Port",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Access Ports block access to larger areas inside of a computer system and allow the player to get past a [partition] barrier."
          }
        ]
      },
      {
        "id": "cabling",
        "name": "Cabling",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Cabling helps wire up aspects of the system together. At the moment it is purely decorative."
          }
        ]
      },
      {
        "id": "divider",
        "name": "Divider",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Dividers are barriers within smaller partitions inside of a system. They block traversal, but do not block line of sight like a [partition] does."
          }
        ]
      },
      {
        "id": "partition",
        "name": "Partition",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Partitions block access from one area to another within a system. Partitions are destructible unless they are adjacent to the edge of a system boundary."
          }
        ]
      },
      {
        "id": "network-port",
        "name": "Network Port",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Network Ports allow access into or out of the system. New arrivals to a system will arrive at that system via the inbound network port. Traffic can then flow out of the system through the outbound network port once the [Firewall] is down."
          }
        ]
      },
      {
        "id": "pool",
        "name": "Thread Pool",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Thread pools don't do a lot other than look pretty at the moment. They are impassible because who wants to spend the time implementing swimming?"
          }
        ]
      },
      {
        "id": "treasure",
        "name": "Treasure Trove",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Treasure troves contain useful supplies and pickups. They may eventually also contain pieces of data and background lore."
          }
        ]
      },
      {
        "id": "service",
        "name": "Service",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Services are currently decorative objects representing a task that the system in question is running. These may eventually effect gameplay or scoring."
          }
        ]
      },
      {
        "id": "queue",
        "name": "Queue",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Queues are decorative tiles at the moment, though the artificial intelligence layer may use them for other aspects of the game later on."
          }
        ]
      },
      {
        "id": "os-core",
        "name": "Operating System Core Node",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "OS Core nodes represent the overall system stability and security. If all of these in a given system can be captured, the player will be granted [admin-access|Admin Acccess] and will be clear to cross the [firewall] to the next area."
          }
        ]
      },
      {
        "id": "firewall",
        "name": "Firewall",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Firewalls protect valuable assets - typically a system or the entire network - from dangerous or unauthorized entities. Firewalls can usually be found guarding [network-port|Network Ports] and will not allow you to cross them until you have [admin-access|Admin Access]."
          }
        ]
      },
      {
        "id": "metadata",
        "name": "Metadata",
        "type": 2,
        "body": [
          {
            "type": "text",
            "content": "Metadata is left over after a destructible object is destroyed. It serves no useful purpoe, other than acting as a reminder of whatever stood in that spot beforehand."
          }
        ]
      }
    ],
    "body": [
      {
        "type": "text",
        "content": "The game world consists of a number of individual levels, each representing a computer in a local area network. Within each level are numerous obstacles, features, and hazards:"
      }
    ]
  },
  {
    "id": "players",
    "header": "Playable Characters",
    "type": 1,
    "topics": [],
    "body": [
      {
        "type": "text",
        "content": "While the player gets to play as a newly-aware artificial intelligence, the exact type of AI they play is is up to the player. Choices have different starting stats and [commands] and will eventually have different perks and drawbacks as well."
      }
    ]
  },
  {
    "id": "actors",
    "header": "Other Characters",
    "type": 0,
    "topics": [],
    "body": [
      {
        "type": "text",
        "content": "Because empty levels are boring, there are a number of other 'actors' within each level, ranging from benign agents of the system to system security looking to attack any intruder (such as yourself), to glitches, bugs, and computer viruses."
      }
    ]
  },
  {
    "id": "commands",
    "header": "Commands",
    "type": 5,
    "topics": [
    ],
    "body": [
      {
        "type": "text",
        "content": "Commands are complex actions that the player can take. If a command is installed (you have it and move it into your installed command bar from the character management screen), you can use it by clicking on it on the main command bar."
      },
      {
        "type": "text",
        "content": "While different [players|characters] start with different sets of commands, commands can also be found in the game world."
      },
      {
        "type": "text",
        "content": "Some commands will require you to specify a target actor or tile in the game world while others will just work."
      },
      {
        "type": "text",
        "content": "Other commands are active commands and will remain on with a persistent effect (typically something modifying the player's capabilities) as long as the player has enough [operations] remaining to keep the command on."
      },
      {
        "type": "text",
        "content": "Currently implemented commands include:"
      }
    ]
  },
  {
    "id": "releases",
    "header": "Release Notes",
    "type": 4,
    "topics": [],
    "body": [
      {
        "type": "text",
        "content": "Starting with version 0.3.0, all updates are accompanied with matching release notes inside of the game. In case you missed a few updates or wanted to go back and see how far the game has come, past release notes are listed below:"
      }
    ]
  }
]
