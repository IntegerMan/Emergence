/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpParagraphComponent } from './help-paragraph.component';

describe('HelpParagraphComponent', () => {
  let component: HelpParagraphComponent;
  let fixture: ComponentFixture<HelpParagraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpParagraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpParagraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
