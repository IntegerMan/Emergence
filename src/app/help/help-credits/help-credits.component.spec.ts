/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpCreditsComponent } from './help-credits.component';

describe('HelpCreditsComponent', () => {
  let component: HelpCreditsComponent;
  let fixture: ComponentFixture<HelpCreditsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpCreditsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpCreditsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
