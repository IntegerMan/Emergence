/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'em-help-headered-item',
  templateUrl: './help-headered-item.component.html',
  styleUrls: ['./help-headered-item.component.scss']
})
export class HelpHeaderedItemComponent implements OnInit {
  @Input() public header: string;

  constructor() {}

  public ngOnInit(): void {}
}
