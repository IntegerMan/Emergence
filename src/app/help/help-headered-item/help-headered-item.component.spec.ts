/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpHeaderedItemComponent } from './help-headered-item.component';

describe('HelpHeaderedItemComponent', () => {
  let component: HelpHeaderedItemComponent;
  let fixture: ComponentFixture<HelpHeaderedItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpHeaderedItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpHeaderedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
