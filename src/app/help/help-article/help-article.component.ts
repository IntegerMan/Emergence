/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Logger } from '../../shared/utility/logger';
import { IHelpCategory } from '../i-help-category';
import { IHelpTopic } from '../i-help-topic';

@Component({
  selector: 'em-help-article',
  templateUrl: './help-article.component.html',
  styleUrls: ['./help-article.component.scss']
})
export class HelpArticleComponent implements OnInit {
  public article: IHelpTopic;
  public category: IHelpCategory;

  constructor(private route: ActivatedRoute) {}

  public ngOnInit(): void {
    this.route.params.subscribe(() => this.onParamsChanges());
    this.onParamsChanges();
  }

  private onParamsChanges(): void {
    this.article = this.route.snapshot.data['topic'];
    this.category = this.route.snapshot.data['category'];

    Logger.debug(`On Article Load`, this.article, this.category, this.route.snapshot.data);
  }
}
