/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { ActorCommand } from '../../libraries/world/actors/commands/actor-command';
import { Logger } from '../../shared/utility/logger';

@Component({
  selector: 'em-command-help',
  templateUrl: './command-help.component.html',
  styleUrls: ['./command-help.component.scss']
})
export class CommandHelpComponent implements OnInit {
  public command: ActorCommand;
  public isActive: boolean;

  constructor(private route: ActivatedRoute) {}

  public ngOnInit(): void {
    this.route.params.subscribe(() => this.onParamsChanges());
    this.onParamsChanges();
  }

  private onParamsChanges(): void {
    this.command = this.route.snapshot.data['command'];
    this.isActive = !isNullOrUndefined((<any>this.command).isActive);

    Logger.debug(`On Command Load`, this.command, this.route.snapshot.data);
  }
}
