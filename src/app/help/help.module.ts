/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpService } from '../services/http.service';
import { SharedModule } from '../shared/shared.module';
import { ActorHelpComponent } from './actor-help/actor-help.component';
import { ArticleNotFoundComponent } from './article-not-found/article-not-found.component';
import { CommandHelpComponent } from './command-help/command-help.component';
import { HelpArticleComponent } from './help-article/help-article.component';
import { HelpCategoryComponent } from './help-category/help-category.component';
import { HelpCreditsComponent } from './help-credits/help-credits.component';
import { HelpHeaderedItemComponent } from './help-headered-item/help-headered-item.component';
import { HelpScreenComponent } from './help-screen/help-screen.component';
import { HelpService } from './help.service';
import { PropertyOrderPipe } from './property-order.pipe';
import { ReleaseDetailsComponent } from './release-details/release-details.component';
import { ReleaseNotesService } from './release-notes.service';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [
    HelpScreenComponent,
    HelpCreditsComponent,
    HelpHeaderedItemComponent,
    HelpArticleComponent,
    ArticleNotFoundComponent,
    ReleaseDetailsComponent,
    CommandHelpComponent,
    ActorHelpComponent,
    HelpCategoryComponent,
    PropertyOrderPipe
  ],
  providers: [HelpService, ReleaseNotesService, PropertyOrderPipe, HttpService]
})
export class HelpModule {}
