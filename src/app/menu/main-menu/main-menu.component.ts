/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { GameLoopService } from '../../services/core-engine/game-loop.service';

@Component({
  selector: 'em-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {
  public version: string;
  public isInGame: boolean;

  constructor(private gameLoopService: GameLoopService) {}

  public ngOnInit(): void {
    this.version = environment.version;
    this.isInGame = this.gameLoopService.isInGame;
  }
}
