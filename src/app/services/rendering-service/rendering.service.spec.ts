/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {RenderingService} from './rendering.service';

describe('RenderingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RenderingService]
    });
  });

  it('should be created', inject([RenderingService], (service: RenderingService) => {
    expect(service).toBeTruthy();
  }));
});
