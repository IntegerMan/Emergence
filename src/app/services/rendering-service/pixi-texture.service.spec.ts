/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {PixiTextureService} from './pixi-texture.service';

describe('PixiTextureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PixiTextureService]
    });
  });

  it('should be created', inject([PixiTextureService], (service: PixiTextureService) => {
    expect(service).toBeTruthy();
  }));
});
