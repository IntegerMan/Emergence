/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import { Point } from '../../libraries/world/point';
import { Logger } from '../../shared/utility/logger';
import { WorldService } from '../core-engine/world.service';

@Injectable()
export class RenderingService {
  public renderInvalidated: EventEmitter<boolean>;

  private _zoomLevel: number = 2;

  constructor(private worldService: WorldService) {
    this.renderInvalidated = new EventEmitter<boolean>();
  }

  get zoomLevel(): number {
    return this._zoomLevel;
  }

  set zoomLevel(value: number) {
    if (this._zoomLevel !== value) {
      Logger.debug(`Zoom level changed to ${value}`);
      this._zoomLevel = value;
      this.renderInvalidated.emit(false);
    }
  }

  public requestRender(isInitialRender: boolean): void {
    this.renderInvalidated.emit(isInitialRender);
  }

  public getRenderCenteringCellPosition(): Point {
    if (this.worldService.player) {
      return this.worldService.player.pos;
    } else {
      return undefined;
    }
  }

  public zoomIn(): void {
    this.zoomLevel = Math.min(3, this.zoomLevel + 0.5);
  }

  public zoomOut(): void {
    this.zoomLevel = Math.max(0.5, this.zoomLevel - 0.5);
  }
}
