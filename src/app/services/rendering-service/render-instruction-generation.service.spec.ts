/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {RenderInstructionGenerationService} from './render-instruction-generation.service';

describe('RenderInstructionGenerationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RenderInstructionGenerationService]
    });
  });

  it('should be created', inject([RenderInstructionGenerationService], (service: RenderInstructionGenerationService) => {
    expect(service).toBeTruthy();
  }));
});
