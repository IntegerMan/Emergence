/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { ActorCommand } from '../../libraries/world/actors/commands/actor-command';
import { ArmorCommand } from '../../libraries/world/actors/commands/armor-command';
import { EscapeCommand } from '../../libraries/world/actors/commands/escape-command';
import { EvadeCommand } from '../../libraries/world/actors/commands/evade-command';
import { MarkCommand } from '../../libraries/world/actors/commands/mark-command';
import { OverclockCommand } from '../../libraries/world/actors/commands/overclock-command';
import { OverloadCommand } from '../../libraries/world/actors/commands/overload-command';
import { RecallCommand } from '../../libraries/world/actors/commands/recall-command';
import { RestoreCommand } from '../../libraries/world/actors/commands/restore-command';
import { ScanCommand } from '../../libraries/world/actors/commands/scan-command';
import { SpikeCommand } from '../../libraries/world/actors/commands/spike-command';
import { StabilizeCommand } from '../../libraries/world/actors/commands/stabilize-command';
import { SurgeCommand } from '../../libraries/world/actors/commands/surge-command';
import { SwapCommand } from '../../libraries/world/actors/commands/swap-command';
import { TargetingCommand } from '../../libraries/world/actors/commands/targeting-command';
import { Logger } from '../../shared/utility/logger';

@Injectable()
export class CommandLookupService {
  private readonly _commands: Map<string, ActorCommand>;

  constructor() {
    this._commands = new Map<string, ActorCommand>();

    this.loadCommands();
  }

  public get commands(): ActorCommand[] {
    const actorCommands: ActorCommand[] = [];

    for (const [key, entry] of Array.from(this._commands.entries())) {
      actorCommands.push(entry);
    }

    Logger.debug(`Get Commands`, actorCommands);

    return actorCommands;
  }

  public findCommand(command: string): ActorCommand | null {
    const commandLower: string = command.toLowerCase();

    Logger.info(`Find command ${commandLower}`, this._commands);

    return this._commands.get(commandLower);
  }

  private loadCommands(): void {
    // Initialize commands with keys so they can be referred to by string in data files
    this.loadCommand('MARK', new MarkCommand());
    this.loadCommand('RECALL', new RecallCommand());
    this.loadCommand('SPIKE', new SpikeCommand());
    this.loadCommand('SURGE', new SurgeCommand());
    this.loadCommand('ARMOR', new ArmorCommand());
    this.loadCommand('EVADE', new EvadeCommand());
    this.loadCommand('TARGETING', new TargetingCommand());
    this.loadCommand('RESTORE', new RestoreCommand());
    this.loadCommand('STABILIZE', new StabilizeCommand());
    this.loadCommand('SWAP', new SwapCommand());
    this.loadCommand('OVERLOAD', new OverloadCommand());
    this.loadCommand('OVERCLOCK', new OverclockCommand());
    this.loadCommand('SCAN', new ScanCommand());
    this.loadCommand('ESCAPE', new EscapeCommand());
  }

  private loadCommand(commandId: string, command: ActorCommand): void {
    this._commands.set(commandId.toLowerCase(), command);
  }
}
