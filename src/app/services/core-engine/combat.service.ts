/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { FovCalculator } from '../../libraries/fov-calculator';
import { Randomizer } from '../../libraries/randomizer';
import { Actor } from '../../libraries/world/actors/actor';
import { CommandContext } from '../../libraries/world/actors/commands/command-context';
import { Cell } from '../../libraries/world/cell';
import { DestructibleObject } from '../../libraries/world/objects/destructible-object';
import { GameObject } from '../../libraries/world/objects/game-object';
import { Point } from '../../libraries/world/point';
import { GoogleAnalyticsHelper } from '../../shared/utility/google-analytics-helper';
import { MessageService } from '../ui-service/message.service';
import { CommandContextService } from './command-context.service';
import { IContextComponent } from './i-context-component';

@Injectable()
export class CombatService implements IContextComponent {
  public static showStats: boolean = true;
  public static showHits: boolean = false;

  constructor(private messageService: MessageService, private contextService: CommandContextService) {
    contextService.registerComponent(this);
  }

  public handleRangedAttack(context: CommandContext, defender: Actor): void {
    // For now, just chain it, but in the future, we'll do something differently
    this.handleAttack(context, defender, true);
  }

  public handleAttack(context: CommandContext, defender: Actor, isRanged: boolean): void {
    // Figure out if the attack lands
    if (!this.isHit(context.invoker, defender)) {
      return;
    }

    const attack: number = context.invoker.getDamageToDeal();
    const defense: number = defender.getDamageResistance();

    const damage: number = attack - defense;

    // Log data to Google Analytics
    if (context.invoker.isPlayer) {
      GoogleAnalyticsHelper.emitEvent('combat', 'player-hit-target', defender.typeId, damage);
      context.invoker.stats.incrementStat('damage-dealt', damage);
    }
    if (defender.isPlayer) {
      GoogleAnalyticsHelper.emitEvent('combat', 'player-was-hit', context.invoker.typeId, damage);
      defender.stats.incrementStat('damage-received', damage);
    }

    let message: string;

    message =
      damage <= 0
        ? `${context.invoker.name} attacks ${defender.name} but deals no damage.`
        : this.hurtActor(context, defender, damage, context.invoker);

    if (message) {
      this.messageService.showMessage(message);
    }

    if (!isRanged && context.invoker.killOnAttack) {
      this.handleActorKamikazi(context);
    }
  }

  public hurtActor(context: CommandContext, defender: Actor, damage: number, attacker: Actor = null): string {
    let captured: boolean = false;

    defender.currentHp = Math.max(0, defender.currentHp - damage);
    defender.wasHurtLastTurn = true;

    if (defender.isDead()) {
      defender.killedBy = attacker;

      if (defender.explosionRadius > 0) {
        const newContext: CommandContext = context.cloneWithNewInvoker(defender);
        this.handleExplosion(newContext, defender.pos, defender.explosionRadius, defender.explosionMinDamage, defender.explosionMaxDamage);
      }

      if (attacker) {
        if (attacker.isPlayer) {
          GoogleAnalyticsHelper.emitEvent('combat', 'killed', defender.typeId);
        }

        attacker.stats.incrementStat('kills');

        // At the moment, MP is only regained by killing things
        attacker.changeOpsPoints(1);
      }

      if (defender.isCapturable) {
        defender.currentHp = defender.maxHp;
        defender.setCapturedBy(attacker);
        captured = true;
      }
    }

    if (defender.isDead()) {
      return `${attacker.name} deals ${damage} damage, killing ${defender.name}.`;
    } else if (captured) {
      if (attacker.faction === defender.faction) {
        return `${attacker.name} does ${damage} damage to ${defender.name}, deallocating it.`;
      } else {
        return `${attacker.name} does ${damage} damage, capturing ${defender.name}.`;
      }
    } else {
      return `${attacker.name} hits ${defender.name} for ${damage} damage.`;
    }
  }

  public handleExplosion(context: CommandContext, epicenter: Point, radius: number, minDamage: number, maxDamage: number): void {
    const candidates: Cell[] = FovCalculator.calculateVisibleCellsFromPoint(epicenter, radius, context.level);
    for (const cell of candidates) {
      const target: Actor = cell.actor;
      if (target && !target.isDead()) {
        const damage: number = Math.max(0, Randomizer.getRandomNumber(minDamage, maxDamage) - target.getDamageResistance());

        if (damage <= 0) {
          context.output(`${target.name} resists all damage from the blast.`);
        } else {
          this.hurtActor(context, target, damage, context.invoker);

          if (target.isDead()) {
            context.output(`${target.name} takes ${damage} damage and is terminated.`);
          } else {
            context.output(`${target.name} is hit for ${damage} damage.`);
          }
        }
      }

      for (const obj of cell.objects.filter((o: GameObject): boolean => o instanceof DestructibleObject && !o.isDestroyed)) {
        const destruct: DestructibleObject = <DestructibleObject>obj;
        const damage: number = Math.max(0, Randomizer.getRandomNumber(minDamage, maxDamage) - destruct.getDamageResistance());

        destruct.damage(damage);

        // This could get spammy if we logged destructions, but it's something to think about.
      }
    }
  }

  private handleActorKamikazi(context: CommandContext): void {
    context.invoker.currentHp = 0;

    if (context.invoker.explosionRadius > 0) {
      this.messageService.showMessage(`${context.invoker.name} explodes!`);

      this.handleExplosion(
        context,
        context.invoker.pos,
        context.invoker.explosionRadius,
        context.invoker.explosionMinDamage,
        context.invoker.explosionMaxDamage
      );
    } else {
      this.messageService.showMessage(`${context.invoker.name} dies after attacking.`);
    }
  }

  private isHit(attacker: Actor, defender: Actor): boolean {
    const attackChance: number = attacker.getAttackHitChance();
    const evadeChance: number = defender.getEvadeChance();

    const hitChance: number = Math.min(Math.max(20, attackChance - evadeChance), 95);

    const roll: number = Randomizer.getWholePercent();
    const isHit: boolean = roll <= hitChance;

    let statString: string = '';
    if (CombatService.showStats && (!isHit || CombatService.showHits)) {
      statString = ` (${attackChance} acc vs ${evadeChance} evade) => Rolled ${roll}, needed ${hitChance} or less`;
    }

    if (!isHit) {
      if (attacker.isPlayer) {
        GoogleAnalyticsHelper.emitEvent('combat', 'player-missed', defender.typeId);
      }
      if (defender.isPlayer) {
        GoogleAnalyticsHelper.emitEvent('combat', 'player-dodged', attacker.typeId);
      }
      this.messageService.showMessage(`${attacker.name} misses ${defender.name}${statString}.`);
    } else if (CombatService.showStats) {
      this.messageService.showMessage(`${attacker.name} hits ${defender.name}${statString}.`);
    }

    return isHit;
  }

  public registerOnContext(context: CommandContext): void {
    context.combatService = this;
  }
}
