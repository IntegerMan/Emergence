/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable, Injector } from '@angular/core';
import { Actor } from '../../libraries/world/actors/actor';
import { CommandContext } from '../../libraries/world/actors/commands/command-context';
import { IContextComponent } from './i-context-component';

/**
 * Constructs CommandContext objects for individual game components to use in interacting with the engine and game world.
 */
@Injectable()
export class CommandContextService {
  private readonly contextComponents: IContextComponent[];

  /**
   * Instantiates the service using Angular's dependency injection.
   * @param {Injector} injector used for delayed construction of services without causing infinite loops in the constructor.
   */
  constructor(private injector: Injector) {
    this.contextComponents = [];
  }

  public registerComponent(contextComponent: IContextComponent): void {
    this.contextComponents.push(contextComponent);
  }

  /**
   * Constructs a command context for the given actor
   * @param {Actor} actor the actor executing the command
   * @returns {CommandContext} the command context.
   */
  public buildCommandContext(actor: Actor): CommandContext {
    const context: CommandContext = new CommandContext();
    context.invoker = actor;

    for (const component of this.contextComponents) {
      component.registerOnContext(context);
    }

    context.isPlayerAction = actor.isPlayer;

    return context;
  }
}
