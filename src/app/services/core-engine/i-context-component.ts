/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommandContext } from '../../libraries/world/actors/commands/command-context';

export interface IContextComponent {
  registerOnContext(context: CommandContext): void;
}
