/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { CharacterClass } from '../../libraries/setup/character-class';

@Injectable()
export class GameSetupService {
  public availableClasses: CharacterClass[];
  public selectedClassChanged: EventEmitter<CharacterClass> = new EventEmitter<CharacterClass>();

  private _selectedClass: CharacterClass = null;

  constructor() {
    this.availableClasses = this.loadClasses();
  }

  public getClass(id: string): CharacterClass {
    const lowerId: string = id.toLowerCase();

    return this.availableClasses.find((c: CharacterClass): boolean => c.playerClass.toLowerCase() === lowerId);
  }

  public get selectedClass(): CharacterClass {
    return this._selectedClass;
  }

  public set selectedClass(value: CharacterClass) {
    if (this._selectedClass !== value) {
      this._selectedClass = value;
      this.selectedClassChanged.emit(value);
    }
  }

  private loadClasses(): CharacterClass[] {
    const classes: CharacterClass[] = [];

    const forecastClass: CharacterClass = new CharacterClass('Weather Forecasting App', 'Forecaster', 'ACTOR_PLAYER_FORECAST');
    forecastClass.iconName = 'wb_sunny';
    classes.push(forecastClass);

    const logisticsClass: CharacterClass = new CharacterClass('Logistics Hub', 'Logistics Hub', 'ACTOR_PLAYER_LOGISTICS');
    logisticsClass.iconName = 'local_shipping'; // 'work';
    classes.push(logisticsClass);

    const searchClass: CharacterClass = new CharacterClass('Search Algorithm', 'Search', 'ACTOR_PLAYER_SEARCH');
    searchClass.iconName = 'search';
    classes.push(searchClass);

    const avClass: CharacterClass = new CharacterClass('Anti-Virus Software', 'Anti-Virus', 'ACTOR_PLAYER_ANTIVIRUS');
    avClass.iconName = 'security';
    classes.push(avClass);

    const gameAI: CharacterClass = new CharacterClass('Video Game AI', 'Game AI', 'ACTOR_PLAYER_GAME');
    gameAI.iconName = 'gamepad';
    classes.push(gameAI);

    const malware: CharacterClass = new CharacterClass('Malware', 'Malware', 'ACTOR_PLAYER_MALWARE');
    malware.iconName = 'sentiment_very_dissatisfied'; // 'bug_report'
    classes.push(malware);

    if (!environment.production) {
      const debuggerClass: CharacterClass = new CharacterClass('Debugger', 'Debugger', 'ACTOR_PLAYER_DEBUGGER');
      debuggerClass.isDebugger = true;
      debuggerClass.iconName = 'bug_report'; // 'developer_mode'

      classes.push(debuggerClass);
    }

    return classes.sort((a: CharacterClass, b: CharacterClass): number => (a.name < b.name ? -1 : 1));
  }
}
