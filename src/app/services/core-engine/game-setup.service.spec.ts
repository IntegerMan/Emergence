/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {GameSetupService} from './game-setup.service';

describe('GameSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameSetupService]
    });
  });

  it('should be created', inject([GameSetupService], (service: GameSetupService) => {
    expect(service).toBeTruthy();
  }));
});
