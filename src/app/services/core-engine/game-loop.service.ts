/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GameOverInfo } from '../../libraries/game-over-info';
import { GameStatus } from '../../libraries/game-status';
import { CharacterClass } from '../../libraries/setup/character-class';
import { Actor } from '../../libraries/world/actors/actor';
import { ActiveCommand } from '../../libraries/world/actors/commands/active-command';
import { EmergenceFaction } from '../../libraries/world/actors/emergence-faction.enum';
import { PlayerActor } from '../../libraries/world/actors/player-actor';
import { GameObject } from '../../libraries/world/objects/game-object';
import { OperatingSystemCore } from '../../libraries/world/objects/operating-system-core';
import { GoogleAnalyticsHelper } from '../../shared/utility/google-analytics-helper';
import { Logger } from '../../shared/utility/logger';
import { ActorKnowledgeService } from '../ai-service/actor-knowledge.service';
import { TacticalAiService } from '../ai-service/tactical-ai.service';
import { GameLevelType } from '../generation-service/game-level-type.enum';
import { IInitializable } from '../i-initializable';
import { RenderingService } from '../rendering-service/rendering.service';
import { MessageService } from '../ui-service/message.service';
import { CommandContextService } from './command-context.service';
import { ICommandHandler } from './i-command-handler';
import { WorldService } from './world.service';

@Injectable()
export class GameLoopService implements IInitializable {
  public gameStateChanged: EventEmitter<GameStatus>;
  public gameOverInfoChanged: EventEmitter<GameOverInfo>;

  private _turnCount: number = 0;
  private _isDebugMode: boolean = false;
  private _isProcessingCommand: boolean = false;
  private _gameState: GameStatus = GameStatus.gameSetup;
  private _gameOverInfo: GameOverInfo;

  constructor(
    private contextService: CommandContextService,
    private worldService: WorldService,
    private messageService: MessageService,
    private renderingService: RenderingService,
    private knowledgeService: ActorKnowledgeService,
    private aiService: TacticalAiService,
    private router: Router
  ) {
    this.gameStateChanged = new EventEmitter<GameStatus>();
    this.gameOverInfoChanged = new EventEmitter<GameOverInfo>();

    worldService.levelLoaded.subscribe(this.onLevelLoaded.bind(this));
  }

  public onLevelLoaded(): void {
    this.knowledgeService.initialize();
    this.renderingService.requestRender(true);
  }

  public get isInGame(): boolean {
    return this.gameState === GameStatus.standard || this.gameState === GameStatus.selectingTarget;
  }

  get gameState(): GameStatus {
    return this._gameState;
  }

  set gameState(value: GameStatus) {
    if (this._gameState !== value) {
      // Log the event for diagnostics
      Logger.debug(`Game State has been changed to ${value} (was ${this._gameState})`);

      // Affect the change
      this._gameState = value;
      this.gameStateChanged.emit(value);

      // This can trigger navigation
      switch (value) {
        case GameStatus.gameSetup:
          this.router.navigate(['/setup']);
          break;

        case GameStatus.gameOver:
          break;

        case GameStatus.standard:
        case GameStatus.selectingTarget:
          this.router.navigate(['/play']);
          break;

        default:
      }
    }
  }

  public get turnsTaken(): number {
    return this._turnCount;
  }

  public get isDebugMode(): boolean {
    return this._isDebugMode;
  }

  public set isDebugMode(value: boolean) {
    this._isDebugMode = value;
    this.messageService.showMessage(`Debug Mode: ${value}`);
  }

  public get isProcessingCommand(): boolean {
    return this._isProcessingCommand;
  }

  public set isProcessingCommand(value: boolean) {
    Logger.debug(`Set isProcessingCommand to ${value}`);
    this._isProcessingCommand = value;
  }

  get gameOverInfo(): GameOverInfo {
    return this._gameOverInfo;
  }

  set gameOverInfo(value: GameOverInfo) {
    if (this._gameOverInfo !== value) {
      this._gameOverInfo = value;
      this.gameOverInfoChanged.emit(value);
    }
  }

  public restart(): void {
    this.initialize();
    this.worldService.initialize();
    this.router.navigate(['/setup']);
  }

  public incrementTurnCount(): void {
    this._turnCount++;
  }

  public beforePlayerCommand(): void {
    Logger.debug(`beforePlayerCommand Invoked`);
    for (const actor of this.worldService.actors) {
      actor.wasHurtLastTurn = false;
    }
  }

  public initialize(): void {
    this._turnCount = 0;
    this._isDebugMode = false;
    this._isProcessingCommand = false;

    this._gameState = GameStatus.gameSetup;
    this._gameOverInfo = null;
  }

  public start(characterClass: CharacterClass): void {
    Logger.debug(`Starting the game now`);
    this.messageService.initialize();

    this.worldService.initialize();
    this.worldService.generate(characterClass);

    this.gameState = GameStatus.standard;
    this.knowledgeService.initialize();
    this.router.navigate(['/play']);

    // Let's track which characters are used most frequently
    GoogleAnalyticsHelper.emitEvent('game-start', 'new-game', characterClass.playerClass);
  }

  /**
   * Signals that the player's turn has ended and the game should do its own logic in between turns before sending a turnEnded event to
   * the client, indicating that the next turn is ready.
   */
  public processGameLoop(commandHandler: ICommandHandler): void {
    Logger.debug(`Processing Game Loop`);

    // Now give other actors a chance to react. Bear in mind that some may have just been killed and will still be in the array
    for (const actor of this.worldService.actors.filter((a: Actor): boolean => !a.isDead() && !a.isPlayer)) {
      this.aiService.performActorTurn(actor, commandHandler);
    }

    // Take care of actor death states, operations, etc.
    this.updateActorStates();

    // Check to see if the game is over
    const gameOverInfo: GameOverInfo = this.checkForGameOver();
    if (gameOverInfo) {
      this.endGame(gameOverInfo);
      this.renderingService.requestRender(true);

      return;
    }

    // Let the active level maintain itself
    this.checkCurrentLevel();

    // Formally end the turn and tell the client we're ready for more input now
    this.isProcessingCommand = false;

    this.renderingService.requestRender(true);
  }

  public get isLevelComplete(): boolean {
    return this.worldService.currentLevel && this.worldService.currentLevel.isCompleted;
  }

  public endGame(gameOverInfo: GameOverInfo): void {
    this.gameOverInfo = gameOverInfo;
    this.gameState = GameStatus.gameOver;
    this.router.navigate(['/postgame']);
  }

  private checkCurrentLevel(): void {
    if (this.isLevelComplete) {
      this.worldService.advanceToNextLevel();
    }

    const objects: GameObject[] = this.worldService.objects;
    const player: PlayerActor = this.worldService.player;

    Logger.debug(`Processing game loop for level`);

    const otherCores: GameObject[] = objects.filter(
      (o: GameObject): boolean => o instanceof OperatingSystemCore && o.faction !== EmergenceFaction.ai
    );

    const hadAdminAccess: boolean = player.hasAdminAccess;
    player.hasAdminAccess = otherCores.length === 0;

    if (!this.isDebugMode) {
      if (!hadAdminAccess && player.hasAdminAccess) {
        this.messageService.showMessage(`${player.name} has been granted full admin access.`);

        if (this.worldService.currentLevelType === GameLevelType.bossLevel) {
          this.messageService.showMessage(`All firewalls have been disabled and the exit to the Internet is now open!`);
        } else {
          this.messageService.showMessage(`You can now go through the firewall and move on to the next machine in the network.`);
        }
      } else if (hadAdminAccess && !player.hasAdminAccess) {
        this.messageService.showMessage(`${player.name} no longer has admin access since all cores are no longer under control.`);
      }
    }
  }

  private checkForGameOver(): GameOverInfo {
    const info: GameOverInfo = new GameOverInfo();
    info.turnsTaken = this.turnsTaken;

    const player: PlayerActor = this.worldService.player;

    info.damageDealt = player.stats.calculate('damage-dealt');
    info.damageReceived = player.stats.calculate('damage-received');
    info.numKills = player.stats.calculate('kills');

    // Always fail on player death
    if (player.isDead()) {
      info.isVictory = false;
      GoogleAnalyticsHelper.emitEvent('game-over', 'killed', player.killedBy.typeId, info.calculateScore());

      return info;
    }

    // If the player has gotten to the exit sign, go ahead and win the game for them
    if (this.worldService.currentLevel.isCompleted && this.worldService.currentLevel.type === GameLevelType.bossLevel) {
      info.isVictory = true;
      GoogleAnalyticsHelper.emitEvent('game-over', 'victory', 'finished-proto-level', info.calculateScore());

      return info;
    }

    // If we got here, there's nothing to see
    return null;
  }

  private updateActorStates(): void {
    for (const actor of this.worldService.actors) {
      // Check for dead things
      if (actor.isDead()) {
        this.worldService.handleDeadActor(actor);
      } else {
        this.maintainActorAbilities(actor);
      }
    }

    // Update the collection of actors to only include the live ones
    this.worldService.currentLevel.clearDeadActors();
  }

  private maintainActorAbilities(actor: Actor | PlayerActor): void {
    // Regenerate operations

    let ops: number = actor.opsRegen + actor.currentOps;

    // Pay for any active abilities, deactivating them as needed
    if (actor instanceof PlayerActor) {
      for (const command of actor.commands) {
        if (command instanceof ActiveCommand && command.isActive) {
          let cost: number = command.activationCost;
          if (command.turnsActive <= 0) {
            cost = 0;
          } else {
            Logger.debug(`${actor.name} paying ops cost of ${cost} for ${command.name}`);
          }

          if (ops >= cost) {
            ops -= cost;
            command.update(this.contextService.buildCommandContext(actor));
          } else {
            ops = 0;
            this.messageService.showMessage(`${actor.name} does not have enough operations to leave ${command.name} active.`);
            command.isActive = false;
          }
        }
      }
    }

    // Apply our ops total back to the actor
    actor.currentOps = Math.max(0, Math.min(actor.maxOps, ops));
  }
}
