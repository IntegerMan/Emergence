/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { EventEmitter, Injectable } from '@angular/core';
import { CommandContext } from '../../libraries/world/actors/commands/command-context';
import { CommandContextService } from '../core-engine/command-context.service';
import { IContextComponent } from '../core-engine/i-context-component';
import { IInitializable } from '../i-initializable';

@Injectable()
export class MessageService implements IInitializable, IContextComponent {
  public messagesChanged: EventEmitter<string[]>;
  public promptChanged: EventEmitter<string>;

  private _prompt: string;
  private readonly _logMessages: string[];

  constructor(private contextService: CommandContextService) {
    this._logMessages = [];
    this._prompt = '';
    this.messagesChanged = new EventEmitter<string[]>();
    this.promptChanged = new EventEmitter<string>();
    this.contextService.registerComponent(this);
  }

  get logMessages(): string[] {
    return this._logMessages;
  }

  get prompt(): string {
    return this._prompt;
  }

  set prompt(value: string) {
    if (this._prompt !== value) {
      this._prompt = value;
      this.promptChanged.emit(value);
    }
  }

  public initialize(): void {
    this._logMessages.length = 0;
    this._prompt = '';
  }

  public showMessage(message: string): void {
    if (message) {
      this._logMessages.push(message);
      this.messagesChanged.emit(this._logMessages);
    }
  }

  public registerOnContext(context: CommandContext): void {
    context.messageService = this;
  }
}
