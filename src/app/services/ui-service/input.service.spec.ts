/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';
import {GameTestingModule} from '../../game-testing/game-testing.module';

import {InputService} from './input.service';

describe('InputService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GameTestingModule]
    });
  });

  it('should be created', inject([InputService], (service: InputService) => {
    expect(service).toBeTruthy();
  }));
});
