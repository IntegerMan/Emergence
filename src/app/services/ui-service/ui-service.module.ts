/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonService } from './button.service';
import { DialogService } from './dialog.service';
import { InputService } from './input.service';
import { MessageService } from './message.service';
import { TooltipService } from './tooltip.service';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [MessageService, TooltipService, ButtonService, DialogService, InputService]
})
export class UiServiceModule {}
