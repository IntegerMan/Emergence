/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';
import {GameTestingModule} from '../../game-testing/game-testing.module';

import {DialogService} from './dialog.service';

describe('DialogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GameTestingModule]
    });
  });

  it(
    'should be created',
    inject([DialogService], (service: DialogService) => {
      expect(service).toBeTruthy();
    })
  );
});
