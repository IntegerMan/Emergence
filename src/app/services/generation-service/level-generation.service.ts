/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Actor } from '../../libraries/world/actors/actor';
import { PlayerActor } from '../../libraries/world/actors/player-actor';
import { GameLevel } from '../../libraries/world/game-level';
import { Cabling } from '../../libraries/world/objects/cabling';
import { DataObject } from '../../libraries/world/objects/data-object';
import { Divider } from '../../libraries/world/objects/divider';
import { Door } from '../../libraries/world/objects/door';
import { Firewall } from '../../libraries/world/objects/firewall';
import { GameObject } from '../../libraries/world/objects/game-object';
import { Metadata } from '../../libraries/world/objects/metadata';
import { NetworkPort } from '../../libraries/world/objects/network-port';
import { OperatingSystemCore } from '../../libraries/world/objects/operating-system-core';
import { Partition } from '../../libraries/world/objects/partition';
import { ServiceObject } from '../../libraries/world/objects/service-object';
import { ThreadPool } from '../../libraries/world/objects/thread-pool';
import { TreasureTrove } from '../../libraries/world/objects/treasure-trove';
import { Turret } from '../../libraries/world/objects/turret';
import { Point } from '../../libraries/world/point';
import { WorldService } from '../core-engine/world.service';
import { HttpService } from '../http.service';
import { BestiaryService } from './bestiary.service';
import { GameLevelType } from './game-level-type.enum';
import { ICellData } from './i-cell-data';
import { ILevel } from './i-level';
import { ILevelGenerationParameters } from './i-level-generation-parameters';
import { ObjectType } from './object-type.enum';

@Injectable()
export class LevelGenerationService {
  constructor(private httpService: HttpService, private bestiaryService: BestiaryService) {}

  public getLevelData(levelID: GameLevelType): Observable<ILevel> {
    return Observable.create((obs: Observer<ILevel>) => {
      const parameters: ILevelGenerationParameters = {
        LevelType: levelID
      };

      this.httpService.post('api/levels', parameters).subscribe((data: ILevel) => {
        obs.next(data);
        obs.complete();
      });
    });
  }

  public generateLevel(worldService: WorldService, player: PlayerActor, levelID: GameLevelType): Observable<GameLevel> {
    return Observable.create((obs: Observer<GameLevel>) => {
      const levelDataObs: Observable<ILevel> = this.getLevelData(levelID);

      levelDataObs.subscribe((levelData: ILevel) => {
        // Create and configure the level
        const level: GameLevel = new GameLevel(levelData, levelID);

        worldService.currentLevel = level;

        // Set up our loot table now
        this.bestiaryService.initialize();
        this.bestiaryService.loadLoot(level, player);

        // Use our level JSON to load the level
        this.addCells(level, levelData.cells);

        level.finalize();

        obs.next(level);
        obs.complete();
      });
    });
  }

  private addCells(level: GameLevel, cells: ICellData[]): void {
    for (const cell of cells) {
      level.addCell(cell);

      if (cell.objects) {
        this.addCellObjects(cell, level);
      }
    }
  }

  private addCellObjects(cell: ICellData, level: GameLevel): void {
    const cellPos: Point = Point.fromPos2D(cell.pos);

    for (const obj of cell.objects) {
      let gameObj: GameObject = null;

      switch (obj.objectType) {
        case ObjectType.Cabling:
          gameObj = new Cabling(level);
          break;
        case ObjectType.Core:
          gameObj = new OperatingSystemCore(level);
          break;
        case ObjectType.DataStore:
          gameObj = new DataObject(level);
          break;
        case ObjectType.Debris:
          gameObj = new Metadata(level);
          break;
        case ObjectType.Divider:
          gameObj = new Divider(level);
          break;
        case ObjectType.Door:
          gameObj = new Door(level);
          break;
        case ObjectType.Entrance:
          gameObj = new NetworkPort(level, true);
          break;
        case ObjectType.Exit:
          gameObj = new NetworkPort(level, false);
          break;
        case ObjectType.Firewall:
          gameObj = new Firewall(level);
          break;
        case ObjectType.Loot:
          gameObj = this.bestiaryService.generateSingleLoot(level); // Actual loot ID should come from the server
          break;
        case ObjectType.Service:
          gameObj = new ServiceObject(level);
          break;
        case ObjectType.Treasure:
          gameObj = new TreasureTrove(level);
          break;
        case ObjectType.Turret:
          gameObj = new Turret(level);
          break;
        case ObjectType.Water:
          gameObj = new ThreadPool(level);
          break;
        case ObjectType.Wall:
          gameObj = new Partition(level, obj.isInvulnerable);
          break;
        case ObjectType.Actor:
          const actor: Actor = this.bestiaryService.generate(obj.objectId);
          actor.pos = cellPos;
          level.actors.push(actor);
          break;
        default:
        // Do nothing
      }

      if (gameObj) {
        level.addObject(gameObj, cellPos);
      }
    }
  }
}
