/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export interface ILevelArrangementInstruction {
  roomId: string;
  x: number;
  y: number;
  flipX: boolean;
  flipY: boolean;
  encounterSet: string;
}
