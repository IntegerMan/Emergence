/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export enum ObjectType {
  /// <summary>
  /// Represents a level core, needed to progress to the next level
  /// </summary>
  Core = 0,
  /// <summary>
  /// A transparent divider that blocks movement
  /// </summary>
  Divider = 1,
  /// <summary>
  /// Floor-based cabling
  /// </summary>
  Cabling = 2,
  /// <summary>
  /// A turret that can fire at nearby targets
  /// </summary>
  Turret = 3,
  /// <summary>
  /// A firewall, blocking access until Cores are taken down
  /// </summary>
  Firewall = 4,
  /// <summary>
  /// The level exit
  /// </summary>
  Exit = 5,
  /// <summary>
  /// The level entrance
  /// </summary>
  Entrance = 6,
  /// <summary>
  /// A service within the level
  /// </summary>
  Service = 7,
  /// <summary>
  /// A data storage device
  /// </summary>
  DataStore = 8,
  /// <summary>
  /// A wall within the level
  /// </summary>
  Wall = 9,
  /// <summary>
  /// Debris from a destroyed object
  /// </summary>
  Debris = 10,
  /// <summary>
  /// A doorway.
  /// </summary>
  Door = 11,
  /// <summary>
  /// Freestanding loot to be collected
  /// </summary>
  Loot = 12,
  /// <summary>
  /// A treasure chest
  /// </summary>
  Treasure = 13,
  /// <summary>
  /// A water tile
  /// </summary>
  Water = 14,
  /// <summary>
  /// An actor in the game world
  /// </summary>
  Actor = 15
}
