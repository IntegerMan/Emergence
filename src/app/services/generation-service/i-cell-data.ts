/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { FloorType } from './floor-type.enum';
import { IObjectData } from './i-object-data';

export interface ICellData {
  pos: string;
  floorType: FloorType;
  objects: IObjectData[];
}
