/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ObjectType } from './object-type.enum';

export interface IObjectData {
  objectType: ObjectType;
  objectId: string;
  isInvulnerable: boolean;
}
