/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {inject, TestBed} from '@angular/core/testing';

import {TacticalAiService} from './tactical-ai.service';

describe('TacticalAiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TacticalAiService]
    });
  });

  it('should be created', inject([TacticalAiService], (service: TacticalAiService) => {
    expect(service).toBeTruthy();
  }));
});
