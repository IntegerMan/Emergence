/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { GameStatus } from '../libraries/game-status';
import { CharacterClass } from '../libraries/setup/character-class';
import { GameLoopService } from '../services/core-engine/game-loop.service';
import { GameSetupService } from '../services/core-engine/game-setup.service';
import { Logger } from '../shared/utility/logger';

@Component({
  selector: 'em-game-setup',
  templateUrl: './game-setup.component.html',
  styleUrls: ['./game-setup.component.scss']
})
export class GameSetupComponent implements OnInit, OnDestroy {
  @Output() public startRequested: EventEmitter<CharacterClass>;

  public availableClasses: CharacterClass[];

  private sub: Subscription;

  constructor(private gameLoopService: GameLoopService, private setupService: GameSetupService, private cdr: ChangeDetectorRef) {
    this.startRequested = new EventEmitter<CharacterClass>();
  }

  public get selectedClass(): CharacterClass {
    return this.setupService.selectedClass;
  }

  public set selectedClass(value: CharacterClass) {
    Logger.debug(`Character class click event detected`, value);
    this.setupService.selectedClass = value;
  }

  public ngOnInit(): void {
    this.sub = this.setupService.selectedClassChanged.subscribe((c: CharacterClass): void => this.onCharacterSelected(c));
    this.availableClasses = this.setupService.availableClasses;
    this.gameLoopService.gameState = GameStatus.gameSetup;
  }

  public ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  public onStartClick(): void {

    Logger.debug(`Start clicked`);

    // Start the game now
    this.gameLoopService.start(this.selectedClass);
  }

  private onCharacterSelected(c: CharacterClass): void {
    this.selectedClass = c;
    this.cdr.detectChanges();
  }
}
