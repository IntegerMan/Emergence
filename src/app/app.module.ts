/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CharacterManagementModule } from './character-management/character-management.module';
import { GameSetupModule } from './game-setup/game-setup.module';
import { HelpModule } from './help/help.module';
import { ActorDefinitionResolver } from './help/resolvers/actor-definition-resolver';
import { CommandResolver } from './help/resolvers/command-resolver';
import { HelpCategoryResolver } from './help/resolvers/help-category-resolver';
import { HelpTopicResolver } from './help/resolvers/help-topic-resolver';
import { ReleaseInfoResolver } from './help/resolvers/release-info-resolver';
import { InGameModule } from './in-game/in-game.module';
import { MenuModule } from './menu/menu.module';
import { CoreEngineModule } from './services/core-engine/core-engine.module';
import { GameSetupService } from './services/core-engine/game-setup.service';
import { CharacterClassResolver } from './shared/character-class-resolver';
import { SharedModule } from './shared/shared.module';
import { RollbarErrorHandler, rollbarFactory, RollbarService } from './shared/utility/rollbar-error-handler';

@NgModule({
  declarations: [AppComponent],
  imports: [CoreEngineModule, HelpModule, GameSetupModule, CharacterManagementModule, InGameModule, MenuModule, SharedModule, CommonModule],
  providers: [
    GameSetupService,
    CharacterClassResolver,
    HelpTopicResolver,
    HelpCategoryResolver,
    ReleaseInfoResolver,
    ActorDefinitionResolver,
    CommandResolver,
    { provide: ErrorHandler, useClass: RollbarErrorHandler },
    { provide: RollbarService, useFactory: rollbarFactory }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
