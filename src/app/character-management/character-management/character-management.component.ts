/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {Component, OnInit} from '@angular/core';
import {DialogService} from '../../services/ui-service/dialog.service';

@Component({
  selector: 'em-character-management',
  templateUrl: './character-management.component.html',
  styleUrls: ['./character-management.component.scss']
})
export class CharacterManagementComponent implements OnInit {
  constructor(private dialogService: DialogService) {}

  public ngOnInit(): void {}

  public onCloseClick(): void {
    this.dialogService.closeActiveDialog();
  }
}
