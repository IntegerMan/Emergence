/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CommandManagementScreenComponent} from './command-management-screen.component';

describe('CommandManagementScreenComponent', () => {
  let component: CommandManagementScreenComponent;
  let fixture: ComponentFixture<CommandManagementScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandManagementScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandManagementScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
