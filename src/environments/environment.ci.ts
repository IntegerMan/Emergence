/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export const environment: any = {
  production: false,
  enableAnalytics: false,
  allowDebugMode: false,
  environmentName: 'Test',
  isHostedContent: false,
  isTestRun: true,
  version: 'Test Version',
  traceAngularRoutes: false,
  baseUrl: 'http://emergenceapidev.azurewebsites.net'
};
