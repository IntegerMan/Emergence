/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export const environment: any = {
  production: true,
  enableAnalytics: true,
  allowDebugMode: false,
  environmentName: 'Itch.io',
  isHostedContent: true,
  isTestRun: false,
  version: 'v{BUILD_VERSION}',
  traceAngularRoutes: false,
  baseUrl: 'https://emergenceapi.azurewebsites.net'
};
