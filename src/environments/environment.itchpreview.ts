/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

export const environment: any = {
  production: true,
  enableAnalytics: true,
  allowDebugMode: false,
  environmentName: 'Itch.io (Preview)',
  isHostedContent: true,
  isTestRun: false,
  version: 'v{BUILD_VERSION} (Preview)',
  traceAngularRoutes: false,
  baseUrl: 'https://emergenceapidev.azurewebsites.net'
};
