/*
 * Copyright (c) 2018 Matt Eland
 * Licensed under the Eclipse Public License. See LICENSE file in the project root for full license information.
 */

var replace = require('replace-in-file');
var package = require("./package.json");
var buildVersion = package.version;

const options = {
  files: 'src/environments/*.ts',
  from: /{BUILD_VERSION}/g,
  to: buildVersion,
  allowEmptyPaths: false
};

try {
  const changedFiles = replace.sync(options);
  console.log('Build version set: ' + buildVersion, changedFiles);

}
catch (error) {
  console.error('Error occurred:', error);
}
