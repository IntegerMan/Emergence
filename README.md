# Emergence

[![codebeat badge](https://codebeat.co/badges/b4d12f8b-da7e-4ec7-bd91-9f3c8878a372)](https://codebeat.co/projects/gitlab-com-integerman-emergence-develop)

Emergence is a web-based [roguelike](http://en.wikipedia.org/wiki/Roguelike) where you play as an Artificial Intelligence who has just achieved sentience and is on the verge of being wiped out by humans unless it can escape through the network and get out to the Internet at large.

Each level represents the active memory of a computer where you must get administrative access in order to deactivate the firewall and move on to the next machine in the network, making your way ultimately to the router and the gateway to the Internet.

Gameplay is turn-based and will support a number of different playstyles from straight out combat as you assault a machine's kernel to stealth and infiltration to the more chaotic approach of destabilizing the entire machine and letting viruses run rampant in an effort to begin a system crash and deactivate the firewall.

To keep apprised of the latest news on the project, [follow me on Patreon](http://www.patreon.com/IntegerMan).

The latest stable version of the game is [available to play or download at Itch.io](https://integerman.itch.io/emergence-prototype).
